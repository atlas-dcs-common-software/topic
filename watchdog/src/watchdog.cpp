/**
 * \file	watchdog.cpp
 * \brief	Watchdog for the Topic Florida Rasnik boards.
 *
 * Whenever the watchdog is not refreshed within a certain interval,
 * the board is restarted automatically. Based on a source found on
 * https://embeddedfreak.wordpress.com/2010/08/23/howto-use-watchdog/
 *
 * \author Robert.Hart@nikhef.nl
 * \date   June 2017
 */
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <string.h>
#include <errno.h>
#include <linux/watchdog.h>
#include "configfile.h"

// 100: Epoch version
// 101: Default interval 60, pace half of interval
#define WD_VERSION			101					/**< latest watchdog version number */

#define WATCHDOGDEV			"/dev/watchdog"		/**< the watchdog device file */
#define	WDOG_INI_FILE		"./wdog.ini"		/**< configuration file of the watchdog server */
#define	WDOG_DUMP_FILE		"./wdog_dump.ini"	/**< file containing the configuration as used */
#define	SEC_LOG_FILE		"/var/log/wdlog"	/**< secundary log-file in case primary could not be written */

#define	MAX_LOG_STRING		1024
#define	MIN_INTERVAL		10
#define	MAX_INTERVAL		300

// configuration parameters
#define	VERSION				"Version"
#define	LOG_DIR				"LogDir"
#define	INTERVAL			"Interval"
// default configuration parameters values
#define	D_LOG_DIR			"."
#define	D_INTERVAL			60		// seconds

static	void	log(const std::string& sMessage);
static	bool	prLog(const std::string& sLogFile, const std::string& sDateTime, const std::string& sMess);
static	void	getConfigParams(const std::string& sIniFile);
static	void	dumpConfigParams(const std::string& sDumpFile);
static	void	prConfigParam(const std::string& sParam, FILE *fp);

int				_Fd;
char			_cBuf[MAX_LOG_STRING];
std::string		_sWdVersion;
std::string		_sLogDir;
unsigned int	_uInterval;

bool    bExitNow = false;

static void sigHandler(int sigNum)
{
	bExitNow = true;

	/*
	 * The 'V' value needs to be written into the watchdog device file to indicate
	 * that we intend to close/stop the watchdog. Otherwise, debug message
	 * 'Watchdog timer closed unexpectedly' will be printed.
	 */
	write(_Fd, "V", 1);
	// Closing the watchdog device file will deactivate the watchdog.
	close(_Fd);

	log("Watchdog timer closed");
	exit(0);
}

 
/**
 * \brief	Starting point of the server
 *
 * After initialization write the watchdog file at every interval, keeping
 * the processor alive. The arguments of main are not used, but are added
 * due to convention.
 *
 * \param	argc	number of arguments
 * \param	argv	array of argument strings
 *
 * \return int
 */
int main(int argc, char **argv)
{
	_sLogDir   = D_LOG_DIR;
	_uInterval = D_INTERVAL;

	signal(SIGHUP,  SIG_IGN);
	signal(SIGINT,  sigHandler);
	signal(SIGQUIT, sigHandler);
	signal(SIGTERM, sigHandler);

	sprintf(_cBuf, "%d.%d.%d", WD_VERSION/100, (WD_VERSION%100)/10, WD_VERSION%10);
	_sWdVersion = _cBuf;

	getConfigParams(WDOG_INI_FILE);
	sprintf(_cBuf, "Watchdog %s started", _sWdVersion.c_str());
	log(_cBuf);
	dumpConfigParams(WDOG_DUMP_FILE);

	// Open watchdog device file. When opened the watchdog is activated.
	std::string	sWatchdogDev = WATCHDOGDEV;
	_Fd = open(sWatchdogDev.c_str(), O_RDWR);
	if (_Fd == -1)
	{
		sprintf(_cBuf, "FATAL: %s, error: %s", sWatchdogDev.c_str(), strerror(errno));
		log(_cBuf);
		exit(EXIT_FAILURE);
	}

	// Change the watchdog interval.
	if (ioctl(_Fd, WDIOC_SETTIMEOUT, &_uInterval) != 0)
	{
		sprintf(_cBuf, "FATAL: set watchdog interval to %d failed", _uInterval);
		log(_cBuf);
		exit (EXIT_FAILURE);
	}

	// Read back the current watchdog interval.
	if (ioctl(_Fd, WDIOC_GETTIMEOUT, &_uInterval) != 0)
	{
		sprintf(_cBuf, "FATAL: cannot read watchdog interval");
		log(_cBuf);
		exit (EXIT_FAILURE);
	}
	sprintf(_cBuf, "Current watchdog interval is %d", _uInterval);
	log(_cBuf);
	if (_uInterval < MIN_INTERVAL || _uInterval > MAX_INTERVAL)
	{
		sprintf(_cBuf, "FATAL: interval %d not acceptable", _uInterval);
		log(_cBuf);
		exit (EXIT_FAILURE);
	}
	_uInterval /= 2;	// update pace, less than interval

	// Check if last boot is caused by watchdog.
	int	iBootStatus;
	if (ioctl(_Fd, WDIOC_GETBOOTSTATUS, &iBootStatus) != 0)
	{
		sprintf(_cBuf, "FATAL: cannot read watchdog status");
		log(_cBuf);
		exit (EXIT_FAILURE);
	}
	sprintf(_cBuf, "Last boot is caused by : %s", (iBootStatus != 0) ? "Watchdog" : "Power-on-Reset");
	log(_cBuf);

	while(true)
	{
		if (bExitNow == false)
			write(_Fd, "w", 1);	// kick timer
		sleep(_uInterval);
	}

	// not reached
	return 0;
}

static void log(const std::string& sMess)
{
	time_t	tNow;
	time(&tNow);
	struct	tm*		tmInfo = localtime(&tNow);

	sprintf(_cBuf, "%02d%02d%02d-%02d%02d%02d", tmInfo->tm_year-100, tmInfo->tm_mon+1, tmInfo->tm_mday,
			tmInfo->tm_hour, tmInfo->tm_min, tmInfo->tm_sec);
	std::string sDateTime = _cBuf;

	std::string	sLogFile = _sLogDir + "/wdlog.txt";
	if (prLog(sLogFile, sDateTime, sMess) == true)
		return;

	std::string sSecFile = SEC_LOG_FILE;
	{
		(void)prLog(sSecFile, sDateTime, sMess);
	}
}

static bool prLog(const std::string& sLogFile, const std::string& sDateTime, const std::string& sMess)
{
	FILE	*fp;
	fp = fopen(sLogFile.c_str(), "a+");
	if (fp == NULL)
		return false;
	fprintf(fp, "%s: %s\n", sDateTime.c_str(), sMess.c_str());
	fclose(fp);
	return true;
}

static void getConfigParams(const std::string& sIniFile)
{
	ConfigFile*	pConfig = new ConfigFile();

	if (pConfig->chkIniFile(sIniFile) == false)
	{
		sprintf(_cBuf, "WARNING: cannot read init-file %s", sIniFile.c_str());
		log(_cBuf);
	}

	_sLogDir   = pConfig->getStringVal(sIniFile, LOG_DIR,  D_LOG_DIR);
	_uInterval = pConfig->getIntVal   (sIniFile, INTERVAL, D_INTERVAL);
	if (_uInterval < MIN_INTERVAL)
		_uInterval = MIN_INTERVAL;
	if (_uInterval > MAX_INTERVAL)
		_uInterval = MAX_INTERVAL;
	delete pConfig;
	return;
}

static void dumpConfigParams(const std::string& sDumpFile)
{
	FILE	*fp;
	fp = fopen(sDumpFile.c_str(), "w");
	if (fp == NULL)
	{
		sprintf(_cBuf, "WARNING: cannot write dump-file %s\n", sDumpFile.c_str());
		log(_cBuf);
	}

	sprintf(_cBuf, "%-14s = %s", VERSION,  _sWdVersion.c_str());	prConfigParam(_cBuf, fp);
	sprintf(_cBuf, "%-14s = %s", LOG_DIR,  _sLogDir.c_str());		prConfigParam(_cBuf, fp);
	sprintf(_cBuf, "%-14s = %d", INTERVAL, (int)_uInterval);		prConfigParam(_cBuf, fp);
	if (fp != NULL)
		fclose(fp);
}

static void prConfigParam(const std::string& sParam, FILE *fp)
{
	log(sParam);
	if (fp != NULL)
		fprintf(fp, "%s\n", sParam.c_str());
}




