// comport.h
//
// COM port communication
//


#define	MAX_BYTES			1024

class ComPort
{
public:
					ComPort(const std::string& portName, const long baudRate, const bool debug);
				   ~ComPort();

	bool			readComPort(std::string& str);
	bool			writeComPort(const std::string& str);

	bool			getStatus()    { return _bStatus;    };
	bool			getDebug()     { return _bDebug;     };
	std::string		getLastError() { return _sLastError; };
	std::string		getComPort()   { return _sComPort;   };
	long			getBaudRate()  { return _dBaudRate;  };

	void			dbgString(const std::string& hdr, const std::string& str);

private:
	int				_Fd;
	bool			_bStatus;
	bool			_bDebug;
	std::string		_sLastError;
	std::string		_sComPort;
	long			_dBaudRate;
	char			_cBuf[MAX_BYTES];
};

