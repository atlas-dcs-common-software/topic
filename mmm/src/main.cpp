/*
 * main.cpp
 */
#include <string>
#include <iostream>
#include <termios.h>
#include "comport.h"

#define	COMPORT		"/dev/ttyPS1"


int main( int argc, char *argv[] )
{
	ComPort*	_pComPort = new ComPort(COMPORT, B115200, true);

	if (_pComPort->getStatus() == false)
	{
		std::cout << "ComPort creation failed: " << _pComPort->getLastError() << std::endl;
		return -1;
	}

	std::string	sInfo;

	while(true)
	{

		std::cin >> sInfo;

		if (sInfo == "q")
			break;

		sInfo += '\r';	// trigger the TopMux

		if (_pComPort->writeComPort(sInfo) == false)
		{
			std::cout << "writeComPort( " << sInfo << " ) failed, " << _pComPort->getLastError() << std::endl;
		}
		else
		{
			while(true)
			{
				if (_pComPort->readComPort(sInfo) == false)
				{
					std::cout << "readComPort( " << sInfo << " ) failed, " << _pComPort->getLastError() << std::endl;
					break;
				}
				else
				{
					if (sInfo == ">")
						break;
				}
			}
		}
	}
	delete _pComPort;

	std::cout << "mmm finished" << std::endl;
	return 0;
}
