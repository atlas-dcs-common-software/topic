/*
 *	comport.cpp ------
 *	Implementation source of class comport.
 *	May 2014, Robert.Hart@nikhef.nl
 */

//#define _CRT_SECURE_NO_WARNINGS 1

#include <string>
#include <string.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstdio>
#include <errno.h>
#include <unistd.h>
#include <termios.h>
#include "comport.h"


ComPort::ComPort(const std::string& portName, const long baudRate, const bool debug)
{
	_bStatus   = false;	// assumption
	_sComPort  = "";
	_dBaudRate = -1;
	_bDebug    = debug;

	if (_bDebug == true)
	{
		std::cout << "[DEBUG]: ComPort( " << portName << ", " << baudRate << " )" << std::endl;
	}

	_Fd = open(portName.c_str(), O_RDWR | O_NOCTTY | O_SYNC);
	if (_Fd < 0)
	{
		sprintf(_cBuf, "error %d opening %s", errno, portName.c_str());
		_sLastError = _cBuf;
		return;
	}

	struct	termios	tty;
	memset(&tty, 0, sizeof tty);

	if (tcgetattr(_Fd, &tty) != 0);
	{
		// warning only, don't know why this call fails
		if (_bDebug == true)
		{
			std::cout << "[DEBUG]: error " << errno << " from tcgetattr" << std::endl;
		}
	}

	cfsetospeed (&tty, baudRate);
	cfsetispeed (&tty, baudRate);
	tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;		// 8-bit chars
	tty.c_iflag &= ~IGNBRK;							// disable break processing
	tty.c_lflag = 0;								// no signalling chars, no echo, no canonical processing
	tty.c_oflag = 0;								// no remapping, no delays
	tty.c_cc[VMIN]  = 0;							// read doesn't block
	tty.c_cc[VTIME] = 5;							// 0.5 seconds read timeout
	tty.c_iflag &= ~(IXON | IXOFF | IXANY);			// shut off xon/xoff ctrl
	tty.c_cflag |= (CLOCAL | CREAD);				// ignore modem controls, enable reading
	tty.c_cflag &= ~(PARENB | PARODD);				// no parity
	tty.c_cflag &= ~CSTOPB;
	tty.c_cflag &= ~CRTSCTS;

	if (tcsetattr(_Fd, TCSANOW, &tty) != 0)
	{
		sprintf(_cBuf, "error %d from tcsetattr", errno);
		_sLastError = _cBuf;
		return;
	}

	_sComPort  = portName;
	_dBaudRate = baudRate;
	_bStatus   = true;
}

ComPort::~ComPort()
{
	if (_Fd >= 0)
		close(_Fd);
	_Fd = -1;
}

bool ComPort::readComPort(std::string& sResult)
{
	if (_bStatus == false)
		return false;

	sResult.clear();

	int				i;
	int				n;
	unsigned char	byte;
	std::string		sInfo = "";

	for(i = 0; i < MAX_BYTES; ++i)
	{
		n = read(_Fd, &byte, 1);
		if (n != 1)
		{
			sprintf(_cBuf, "ERROR readComPort(%s): read failed %d", sInfo.c_str(), n);
			_sLastError = _cBuf;
			return false;
		}

		sInfo += byte;

		if (byte == '\r')
			continue;
		if (byte == '\n')
			break;
		sResult += byte;
	}

	if (i >= MAX_BYTES)
	{
		sprintf(_cBuf, "ERROR readComPort(%s): illegal length %d", sInfo.c_str(), i);
		_sLastError = _cBuf;
		return false;
	}

	dbgString("r", sInfo);

	return true;
}

bool ComPort::writeComPort(const std::string& sSource)
{
	if (_bStatus == false)
		return false;

	std::string		sInput = sSource;

	int				i;
	int				n;
	unsigned char	bytes[MAX_BYTES];
	int				len = (int)sInput.size();

	if (len <= 0 || len > (MAX_BYTES-1))
	{
		sprintf(_cBuf, "ERROR writeComPort: illegal length %d", len);
		_sLastError = _cBuf;
		return false;
	}

	dbgString("w", sInput);

	for(i = 0; i < (int)len; ++i)
		bytes[i] = sInput[i];

	n = write(_Fd, bytes, len);
	if (n != len)
	{
		sprintf(_cBuf, "ERROR writeComPort: len=%d, n=%d", len, n);
		_sLastError = _cBuf;
		return false;
	}

	return true;
}

void ComPort::dbgString(const std::string& hdr, const std::string& str)
{
	if (_bDebug == false)
		return;

	unsigned int	i;
	unsigned int	dLen = str.size();

	if (dLen == 0)
	{
		std::cout << hdr << ": <null>" << std::endl;
		return;
	}

	std::string	sResult;

	sResult = "";
	for(i = 0; i < dLen; ++i)
	{
		if (str[i] == '\r')
			sResult += "<CR>";
		else if (str[i] == '\n')
			sResult += "<LF>";
		else
			sResult += str[i];
	}
	std::cout << hdr << ": " << sResult << std::endl;
}
