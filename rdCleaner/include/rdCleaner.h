/**
 *  \file   rdCleaner.h
 *  \brief  Include file as used by the rdCleaner application
 *  \author Robert.Hart@nikhef.nl
 *  \date   April 2018
 */

#ifndef RDCLEANER_H_
#define RDCLEANER_H_

// 100: Epoch version
#define RDCL_VERSION        101                  /**< latest rdCleaner version number */

#define CLEANER_INI_FILE    "./cleaner.ini"      /**< configuration file of the rdCleaner application */
#define CLEANER_DUMP_FILE   "./cleaner_dump.ini" /**< file containing the configuration as used */
#define SEC_LOG_FILE        "/var/log/cllog"     /**< Secondary log-file in case primary could not be written */

#define MAX_LOG_STRING      1024
#define MAX_LINE            1024

#define DATA_DEVICE         "mmcblk0p3"
#define DF_TMP_FILE         "/tmp/df-usage"

#define MIN_INTERVAL        600     // 10 minutes
#define MAX_INTERVAL        21600   // 6 hours

#define MIN_MONTHS          1
#define MAX_MONTHS          12      // 1 year

#define MIN_CLEANING_DAY    1
#define MAX_CLEANING_DAY    28

#define MIN_DISK_USAGE      50
#define MAX_DISK_USAGE      90

// configuration parameters
#define VERSION             "Version"
#define LOG_DIR             "LogDir"
#define INTERVAL            "Interval"
#define MONTHS              "Months"
#define CLEANING_DAY        "CleaningDay"
#define DISK_USAGE          "DiskUsage"
#define RD_LOG_DIR          "RdLogDir"
#define RD_IMAGE_DIR        "RdImageDir"
#define RD_RESULT_DIR       "RdResultDir"
#define RD_LOG_HDR          "RdLogHdr"
#define RD_IMAGE_HDR        "RdImageHdr"
#define RD_RESULT_HDR       "RdResultHdr"
// default configuration parameter values
#define D_LOG_DIR           "/balign/data/log/cllog"
#define D_INTERVAL          3600    // 1 hour
#define D_MONTHS            2       // files older than will be removed
#define D_CLEANING_DAY      15      // 15th of each month
#define D_DISK_USAGE        80      // if more, reboot
#define D_RD_LOG_DIR        "/balign/data/log"
#define D_RD_IMAGE_DIR      "/balign/data/images"
#define D_RD_RESULT_DIR     "/balign/data/results"
#define D_RD_LOG_HDR        "rdlog_"
#define D_RD_IMAGE_HDR      "img"
#define D_RD_RESULT_HDR     "rd_"

#endif /* RDCLEANER_H_ */
