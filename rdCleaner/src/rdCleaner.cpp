/**
 *  \file   rdCleaner.cpp
 *  \author Robert.Hart@nikhef.nl
 *  \date   April 2018
 *  \brief  The \e rdCleaner server removes at regular inetervals old log files,
 *          result files and image files, as created by the \e Rasdim server on
 *          the Topic boards. It also restarts the processor if the data device
 *          becomes almost full.
 */

/**
 *  \mainpage [rdCleaner]
 *  \author Robert.Hart@nikhef.nl
 *  \date   April 2018
 */

#include <iostream>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include "rdCleaner.h"
#include "configfile.hxx"

static  void    log(const std::string& sMessage);
static  bool    prLog(const std::string& sLogFile, const std::string& sDateTime, const std::string& sMess);
static  void    getConfigParams(const std::string& sIniFile);
static  void    dumpConfigParams(const std::string& sDumpFile);
static  void    prConfigParam(const std::string& sParam, FILE *fp);
static  void    scanDir4Files(const std::string& sDir, const std::string& sHdr, const int nCurMonths);
static  void    scanDir4Dirs (const std::string& sDir, const std::string& sHdr, const int nCurMonths);
static  bool    chkExpDate(const int iDate, const int nCurMonths);
static  bool    emptyDirectory(const std::string& sDir);
static  bool    checkDiskFree();

// main parameters
char            _cBuf[MAX_LOG_STRING];
std::string     _sVersion;
std::string     _sLogDir;
unsigned int    _uInterval;
unsigned int    _uMonths;
unsigned int    _uCleaningDay;
unsigned int    _uDiskUsage;
std::string     _sRdLogDir;
std::string     _sRdImageDir;
std::string     _sRdResultDir;
std::string     _sRdLogHdr;
std::string     _sRdImageHdr;
std::string     _sRdResultHdr;

bool    bExitNow = false;   /**< Set true if a signal is raised */

void sigHandler(int sigNum)
{
    bExitNow = true;
    log("rdCleaner stopped by signal");
    exit(0);
}

/**
 * \brief   Starting point of the rdCleaner application
 *
 * After initialization check at regular intervals if 'old' files has to be removed.
 * The arguments of main are not used, but are added due to convention.
 *
 * \param   argc    number of arguments
 * \param   argv    array of argument strings
 *
 * \return  int
 */
int main( int argc, char *argv[] )
{
    signal(SIGHUP,  SIG_IGN);
    signal(SIGINT,  sigHandler);
    signal(SIGQUIT, sigHandler);
    signal(SIGTERM, sigHandler);

    sprintf(_cBuf, "%d.%d.%d", RDCL_VERSION/100, (RDCL_VERSION%100)/10, RDCL_VERSION%10);
    _sVersion = _cBuf;

    getConfigParams(CLEANER_INI_FILE);
    sprintf(_cBuf, "rdCleaner %s started", _sVersion.c_str());
    log(_cBuf);
    dumpConfigParams(CLEANER_DUMP_FILE);

    int dCleanDay = 0;

    while(true)
    {
    	sleep(_uInterval);
    	if (checkDiskFree() == false)
    		break;

    	time_t  tNow;
    	time(&tNow);
    	struct  tm* tInfo  = localtime(&tNow);

    	if (tInfo->tm_mday == (int)_uCleaningDay)
    	{
    		++dCleanDay;
    		if (dCleanDay == 1)
    		{
    			log("Cleaning the data area");
    			int nCurMonths = (tInfo->tm_year + 1900) * 12 + tInfo->tm_mon + 1;
    			scanDir4Files(_sRdLogDir,    _sRdLogHdr,    nCurMonths);
    			scanDir4Files(_sRdResultDir, _sRdResultHdr, nCurMonths);
    			scanDir4Dirs (_sRdImageDir,  _sRdImageHdr,  nCurMonths);
    		}
    	}
    	else
    		dCleanDay = 0;
    }

    // checkDiskFree failed, reboot system
    log("Not enough disk space on data device, system will be rebooted");
    std::string sCommand = "shutdown -r now";
    if (system(sCommand.c_str()) < 0)
    {
    	sprintf(_cBuf, "Failed to execute: %s", sCommand.c_str());
    	log(_cBuf);
    	return 0;
    }

    sleep (10);	// during this sleep the system will be restarted
    return 0;
}

static bool checkDiskFree()
{
	std::string sDfTmpFile = DF_TMP_FILE;

	sprintf(_cBuf, "df | grep %s > %s", DATA_DEVICE, sDfTmpFile.c_str());
	std::string sCommand = _cBuf;

	if (system(sCommand.c_str()) < 0)
	{
	    sprintf(_cBuf, "checkDiskFree: failed to execute: %s", sCommand.c_str());
	    log(_cBuf);
	    return true;    // we don't want to reboot if this command fails
	}

	FILE    *fp;
	fp = fopen(sDfTmpFile.c_str(), "r");
	if (fp == NULL)
	{
		sprintf(_cBuf, "checkDiskFree: cannot open %s", sDfTmpFile.c_str());
		log(_cBuf);
		return true;    // we don't want to reboot if this open fails
	}

	int     iLine   = 0;
	bool    bResult = true;
	int     iFraction;
	char    sLine[MAX_LINE];

	while( fgets(sLine, MAX_LINE, fp) != NULL )
	{
		++iLine;

        int n = sscanf(sLine, "%*s %*d %*d %*d %d%% %*s", &iFraction);
        if (n != 1)
        	bResult = false;
        break;
	}
	fclose(fp);

	if (remove(sDfTmpFile.c_str()) < 0)
	{
	    sprintf(_cBuf, "checkDiskFree: file %s could not be removed, errno %d (%s)", sDfTmpFile.c_str(), errno, strerror(errno));
		log(_cBuf);
	}

	if (iLine != 1)
	{
		sprintf(_cBuf, "checkDiskFree: empty file %s", sDfTmpFile.c_str());
		log(_cBuf);
		return true;

	}
	if (bResult == false)
	{
		sprintf(_cBuf, "checkDiskFree: format error in line %s", sLine);
		log(_cBuf);
		return true;
	}
	if (iFraction < 0 || iFraction > 100)
	{
		sprintf(_cBuf,"checkDiskFree: illegal fraction %d in line %s", iFraction, sLine);
		log(_cBuf);
		return true;
	}

	if (iFraction > (int)_uDiskUsage)
	{
		sprintf(_cBuf, "checkDiskFree: disk space fraction %d exceeds limit %d", iFraction, _uDiskUsage);
		log(_cBuf);
		return false;   // reboot
	}

	sprintf(_cBuf, "checkDiskFree: fraction %d still okay", iFraction);
	log(_cBuf);
	return true;
}

static void scanDir4Files(const std::string& sDir, const std::string& sHdr, const int nCurMonths)
{
	sprintf(_cBuf, "Checking %s for %s files", sDir.c_str(), sHdr.c_str());
	log(_cBuf);

	sprintf(_cBuf, "%s%%d.txt", sHdr.c_str());
	std::string  sFormat = _cBuf;

    DIR*    pDir = opendir(sDir.c_str());

    if (pDir == NULL)
    {
    	sprintf(_cBuf, "cannot open %s", sDir.c_str());
    	log(_cBuf);
    	return;
    }

    int                 iDate;
    struct  dirent*     pEnt;
    std::string         sFile;

    while((pEnt = readdir(pDir)) != NULL)
    {
    	sFile = pEnt->d_name;
    	if (sFile == "." || sFile == "..")
            continue;
    	if (pEnt->d_type != DT_REG)
    		continue;

    	int n = sscanf(sFile.c_str(), sFormat.c_str(), &iDate);
    	if (n == 1)
    	{
    		bool    bRemove = chkExpDate(iDate, nCurMonths);

    		if (bRemove == true)
    		{
    			std::string sRemoveFile = sDir + "/" + sFile;

    			if (remove(sRemoveFile.c_str()) < 0)
    			{
    				sprintf(_cBuf, "file %s could not be removed, errno %d (%s)", sRemoveFile.c_str(), errno, strerror(errno));
    				log(_cBuf);
    			}
    		}
    	}
    }

    closedir(pDir);
}

static void scanDir4Dirs(const std::string& sDir, const std::string& sHdr, const int nCurMonths)
{
	sprintf(_cBuf, "Checking %s for %s directories", sDir.c_str(), sHdr.c_str());
    log(_cBuf);

    sprintf(_cBuf, "%s%%d.txt", sHdr.c_str());
    std::string  sFormat = _cBuf;

    DIR*    pDir = opendir(sDir.c_str());

    if (pDir == NULL)
    {
    	sprintf(_cBuf, "cannot open %s", sDir.c_str());
    	log(_cBuf);
    	return;
    }

    int                 iDate;
    struct  dirent*     pEnt;
    std::string         sDirectory;

    while((pEnt = readdir(pDir)) != NULL)
    {
    	sDirectory = pEnt->d_name;
    	if (sDirectory == "." || sDirectory == "..")
            continue;
    	if (pEnt->d_type != DT_DIR)
    		continue;

    	int n = sscanf(sDirectory.c_str(), sFormat.c_str(), &iDate);
    	if (n == 1)
    	{
    	    bool    bRemove = chkExpDate(iDate, nCurMonths);

    	    if (bRemove == true)
    	    {
    	        std::string sRemoveDir = sDir + "/" + sDirectory;

    	        if (emptyDirectory(sRemoveDir) == true)
    	        {
    	            if (remove(sRemoveDir.c_str()) < 0)
    	            {
    	                sprintf(_cBuf, "directory %s could not be removed, errno %d (%s)", sRemoveDir.c_str(), errno, strerror(errno));
    	                log(_cBuf);
    	            }
    	        }
    	    }
        }
    }

    closedir(pDir);
}

static bool emptyDirectory(const std::string& sDir)
{
	DIR*    pDir = opendir(sDir.c_str());

    if (pDir == NULL)
	{
        sprintf(_cBuf, "cannot open %s", sDir.c_str());
        log(_cBuf);
        return false;
    }

    struct  dirent*     pEnt;
    std::string         sFile;
    bool                bResult = true;

    while((pEnt = readdir(pDir)) != NULL)
    {
        sFile = pEnt->d_name;
        if (sFile == "." || sFile == "..")
	        continue;
	    if (pEnt->d_type != DT_REG)
	    	continue;

	    std::string sRemoveFile = sDir + "/" + sFile;

	    if (remove(sRemoveFile.c_str()) < 0)
	    {
	    	bResult = false;
	        sprintf(_cBuf, "file %s could not be removed, errno %d (%s)", sRemoveFile.c_str(), errno, strerror(errno));
	    	log(_cBuf);
	    }
    }
    closedir(pDir);
    return bResult;
}

static bool chkExpDate(const int iDate, const int nCurMonths)
{
	int iYear  = iDate / 10000;
	int iMonth = (iDate % 10000) / 100;
//  int iDay   = iDate % 100;

	int nFileMonths = iYear * 12 + iMonth;
	int nDiffMonths = nCurMonths - nFileMonths;

	if (nDiffMonths >= (int)_uMonths)
        return true;
	return false;
}

static void log(const std::string& sMess)
{
	time_t	tNow;
	time(&tNow);
	struct	tm*		tmInfo = localtime(&tNow);

	sprintf(_cBuf, "%02d%02d%02d-%02d%02d%02d", tmInfo->tm_year+1900, tmInfo->tm_mon+1, tmInfo->tm_mday,
			tmInfo->tm_hour, tmInfo->tm_min, tmInfo->tm_sec);
	std::string sDateTime = _cBuf;

	std::string	sLogFile = _sLogDir + "/cllog.txt";
	if (prLog(sLogFile, sDateTime, sMess) == true)
		return;

	std::string sSecFile = SEC_LOG_FILE;
	{
		(void)prLog(sSecFile, sDateTime, sMess);
	}
}

static bool prLog(const std::string& sLogFile, const std::string& sDateTime, const std::string& sMess)
{
	FILE	*fp;
	fp = fopen(sLogFile.c_str(), "a+");
	if (fp == NULL)
		return false;
	fprintf(fp, "%s: %s\n", sDateTime.c_str(), sMess.c_str());
	fclose(fp);
	return true;
}

static void getConfigParams(const std::string& sIniFile)
{
	ConfigFile*	pConfig = new ConfigFile();

	if (pConfig->chkIniFile(sIniFile) == false)
	{
		sprintf(_cBuf, "WARNING: cannot read config-file %s", sIniFile.c_str());
		log(_cBuf);
	}
	_sLogDir      = pConfig->getStringVal(sIniFile, LOG_DIR,       D_LOG_DIR);
	_uInterval    = pConfig->getIntVal   (sIniFile, INTERVAL,      D_INTERVAL);
	_uMonths      = pConfig->getIntVal   (sIniFile, MONTHS,        D_MONTHS);
	_uCleaningDay = pConfig->getIntVal   (sIniFile, CLEANING_DAY,  D_CLEANING_DAY);
	_uDiskUsage   = pConfig->getIntVal   (sIniFile, DISK_USAGE,    D_DISK_USAGE);
	_sRdLogDir    = pConfig->getStringVal(sIniFile, RD_LOG_DIR,    D_RD_LOG_DIR);
	_sRdImageDir  = pConfig->getStringVal(sIniFile, RD_IMAGE_DIR,  D_RD_IMAGE_DIR);
	_sRdResultDir = pConfig->getStringVal(sIniFile, RD_RESULT_DIR, D_RD_RESULT_DIR);
	_sRdLogHdr    = pConfig->getStringVal(sIniFile, RD_LOG_HDR,    D_RD_LOG_HDR);
    _sRdImageHdr  = pConfig->getStringVal(sIniFile, RD_IMAGE_HDR,  D_RD_IMAGE_HDR);
    _sRdResultHdr = pConfig->getStringVal(sIniFile, RD_RESULT_HDR, D_RD_RESULT_HDR);
	delete pConfig;

	if (_uInterval < MIN_INTERVAL)
        _uInterval = MIN_INTERVAL;
    if (_uInterval > MAX_INTERVAL)
        _uInterval = MAX_INTERVAL;

    if (_uMonths < MIN_MONTHS)
    	_uMonths = MIN_MONTHS;
    if (_uMonths > MAX_MONTHS)
    	_uMonths = MAX_MONTHS;

    if (_uCleaningDay < MIN_CLEANING_DAY)
        _uCleaningDay = MIN_CLEANING_DAY;
    if (_uCleaningDay > MAX_CLEANING_DAY)
        _uCleaningDay = MAX_CLEANING_DAY;

    if (_uDiskUsage < MIN_DISK_USAGE)
    	_uDiskUsage = MIN_DISK_USAGE;
    if (_uDiskUsage > MAX_DISK_USAGE)
    	_uDiskUsage = MAX_DISK_USAGE;
}

static void dumpConfigParams(const std::string& sDumpFile)
{
	FILE	*fp;
	fp = fopen(sDumpFile.c_str(), "w");
	if (fp == NULL)
	{
		sprintf(_cBuf, "WARNING: cannot write dump-file %s\n", sDumpFile.c_str());
		log(_cBuf);
	}

	sprintf(_cBuf, "%-14s = %s", VERSION,       _sVersion.c_str());	    prConfigParam(_cBuf, fp);
	sprintf(_cBuf, "%-14s = %s", LOG_DIR,       _sLogDir.c_str());		prConfigParam(_cBuf, fp);
	sprintf(_cBuf, "%-14s = %d", INTERVAL,      (int)_uInterval);		prConfigParam(_cBuf, fp);
	sprintf(_cBuf, "%-14s = %d", MONTHS,        (int)_uMonths);		    prConfigParam(_cBuf, fp);
	sprintf(_cBuf, "%-14s = %d", CLEANING_DAY,  (int)_uCleaningDay);    prConfigParam(_cBuf, fp);
	sprintf(_cBuf, "%-14s = %d", DISK_USAGE,    (int)_uDiskUsage);      prConfigParam(_cBuf, fp);
	sprintf(_cBuf, "%-14s = %s", RD_LOG_DIR,    _sRdLogDir.c_str());    prConfigParam(_cBuf, fp);
	sprintf(_cBuf, "%-14s = %s", RD_IMAGE_DIR,  _sRdImageDir.c_str());  prConfigParam(_cBuf, fp);
	sprintf(_cBuf, "%-14s = %s", RD_RESULT_DIR, _sRdResultDir.c_str()); prConfigParam(_cBuf, fp);
	sprintf(_cBuf, "%-14s = %s", RD_LOG_HDR,    _sRdLogHdr.c_str());    prConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %s", RD_IMAGE_HDR,  _sRdImageHdr.c_str());  prConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %s", RD_RESULT_HDR, _sRdResultHdr.c_str()); prConfigParam(_cBuf, fp);
	if (fp != NULL)
		fclose(fp);
}

static void prConfigParam(const std::string& sParam, FILE *fp)
{
	log(sParam);
	if (fp != NULL)
		fprintf(fp, "%s\n", sParam.c_str());
}


