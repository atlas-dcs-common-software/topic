/**
 *  \file   muxunit.cpp
 *  \brief  Implementation source of the MuxUnit class.
 *  \author Robert.Hart@nikhef.nl
 *  \date   Feb 2016, implemented on TOPIC
 */

#include <string>
#include <stdio.h>
#include "muxunit.h"
#include "comport.h"
#include "server.h"

// command definitions/syntax
#define MUX_RESET       "O"
#define MUX_PUR         "pur"
#define SELECT_LED      "L%1x%1x%1x"
#define SELECT_CAM      "C%1x%1x%1x"
#define GET_MUX_VERSION "V"
#define GET_MUX_CURRENT "S"
#define GET_I2C_SETTING "R"
#define I2C_SETUP1      "W1%03x"
#define I2C_SETUP2      "W2%03x"
#define I2C_SETUP3      "WE%03x"
#define I2C_ANALOGUE    "WA%03x"
#define I2C_GAIN        "W5%03x"
#define I2C_COARSE      "W3%03x"
#define I2C_FINE        "W4%03x"
#define I2C_LOWER       "W8%03x"
#define I2C_UPPER       "W9%03x"

// response definitions
#define SEL_CAM_OK      "Clock: cont"   /**< Okay string for select camera */
#define I2C_REG_OK      "ok"            /**< Okay string for I2C setting */
#define ACK             ">"             /**< Synchronization prompt */

#define BIT_0           0x1
#define BIT_1           0x2
#define BIT_2           0x4
#define BIT_3           0x8
#define BIT_4           0x10
#define BIT_5           0x20
#define BIT_6           0x40
#define BIT_7           0x80
#define BIT_8           0x100
#define BIT_9           0x200
#define BIT_10          0x400
#define BIT_11          0x800

#define MAX_TRY         16      /**< Maximum tries of reads in for loop for #waitForAck */
#define MIN_LED_CURRENT 20      /**< Minimum current for LED if switched on */

/**
 *  \brief  The (only) constructor of the MuxUnit class.
 *
 *  The main task of this constructor is to create an instance of the ComPort class.
 *  If successful the #_bStatus flag is set to true.
 *
 *  \param  portName name of the serial port connected to the TopMux
 *  \param  baudRate communications speed in bits/sec
 *  \param  debug on if true, off otherwise
 *  \param  pServer pointer to an instance (caller) of the Server class, used only for its #Server.log method
 *
 *  \return void
 */
MuxUnit::MuxUnit(const std::string& portName, const long baudRate, const bool debug, Server* pServer)
{
    _bStatus  = false;  // assumption
    _bDebug   = debug;
    _pServer  = pServer;
    _pComPort = new ComPort(portName, baudRate, debug, pServer);

    if (_pComPort->getStatus() == false)
    {
        _sLastError = _pComPort->getLastError();
        return;
    }
    _sComPort = portName;
    _bStatus  = true;
}

/**
 *  \brief  Destructor of the MuxUnit class. It deletes the ComPort instance.
 *  \return void
 */
MuxUnit::~MuxUnit()
{
    if (_bStatus == true)
        delete _pComPort;
}

/**
 *  \brief  Sends a command to the TopMux.
 *
 *  The command string is send to the TopMux by means of the writeComPort method
 *  of the ComPort class. NB: A carriage return is added to the command string, it triggers
 *  the firmware of the TopMux. The first line send back, will be the echoed command, which
 *  is read back by the receiveLine method. On error false is returned, true otherwise.
 *
 *  \param  sCmd the command
 *  \return bool
 */
bool MuxUnit::issueCommand(const std::string& sCmd)
{
    _sLastError.clear();

    std::string sCommand;
    sCommand  = sCmd;
    sCommand += '\r';   // activate TopMux

    if (_pComPort->writeComPort(sCommand) == false)
    {
        sprintf(_cBuf, "Error on writeComPort(%s): [%s]", sCmd.c_str(), _pComPort->getLastError().c_str());
        _sLastError = _cBuf;
        return false;
    }

    for (int i = 0; i < MAX_TRY; ++i)
    {
        std::string sLine;
        if (this->receiveLine(sLine) == false)
            return false;
        // if (sLine == sCmd)
        // somehow the command is not echoed properly (RH: Feb 2016)
        return true;
    }
    return false;
}

/**
 *  \brief  Read a single (non-empty) line from the TopMux.
 *
 *  The readComPort method of the ComPort instance is used to receive a string from the TopMux.
 *  Sometimes the TopMux generates empty lines. They are ignored and the TopMux is read again.
 *  On error false is returned, true otherwise. On success true is returned, false otherwise.
 *
 *  \param  sLine the string read from the TopMux
 *  \return bool
 */
bool MuxUnit::receiveLine(std::string& sLine)
{
    /*
     * Sometimes the TopMux generates empty lines.
     * Ignore these and read again.
     * RH: Feb. 26 2016
     */
    while(true)
    {
        if (_pComPort->readComPort(sLine) == false)
        {
            sprintf(_cBuf, "Error on readComPort(%s): [%s]", sLine.c_str(), _pComPort->getLastError().c_str());
            _sLastError = _cBuf;
            return false;
        }
        if (sLine.size() > 0)
            return true;
    }
}

/**
 *  \brief  Read from TopMux until ACK string.
 *
 *  To order to synchronize between command and response, the last string send by the TopMux is
 *  a line with the #ACK string. So, read until found (but not more than #MAX_TRY times).
 *  On error false is returned, true otherwise.
 *
 *  \param  sAckLine container and result of all lines read until the #ACK line
 *  \return bool
 */
bool MuxUnit::waitForAck(std::string& sAckLine)
{
    for(int i = 0; i < MAX_TRY; ++i)
    {
        std::string sLine;

        if (this->receiveLine(sLine) == false)
            return false;
        if (sLine == ACK)
            return true;
        if (!sAckLine.empty())
            sAckLine += ", ";
        sAckLine += sLine;
    }
    return false;
}

/**
 *  \brief  Reset the TopMux. On error false is returned, true otherwise.
 *
 *  \return bool
 */
bool MuxUnit::resetMux()
{
    if (this->issueCommand(MUX_RESET) == false)
        return false;
    std::string sAckLine;
    return this->waitForAck(sAckLine);
}

/**
 *  \brief  Select camera.
 *
 *  The 3 address parameters of the camera are encoded into the command string. After sending the command,
 *  a result line is read and compared with the string #SEL_CAM_OK. On error false is returned,
 *  true otherwise.
 *
 *  \param  topCam output number to MasterMux
 *  \param  masCam output number to RasMux
 *  \param  rasCam output number to camera
 *
 *  \return bool
 */
bool MuxUnit::selectCam(const int topCam, const int masCam, const int rasCam)
{
    sprintf(_cCmd, SELECT_CAM, topCam, masCam, rasCam);
    if (this->issueCommand(_cCmd) == false)
        return false;

    std::string sLine;
    if (this->receiveLine(sLine) == false)
        return false;

    bool    result = true;
    if (sLine != SEL_CAM_OK)
    {
        _sLastError = sLine;
        result = false;
    }

    std::string sAckLine;
    (void)this->waitForAck(sAckLine);
    _sLastError += sAckLine;

    return result;
}

/**
 *  \brief  Select a light source (led).
 *
 *  The 3 address parameters of the led are encoded into the command string. After sending the command,
 *  a result line is read, containing the current of the light source. If the current does
 *  not exceed the #MIN_LED_CURRENT value, the selection is considered not successful.
 *  On error false is returned, true otherwise.
 *
 *  \param  topLed output number to MasterMux
 *  \param  masLed output number to RasMux
 *  \param  rasLed output number to light source
 *
 *  \return bool
 */
bool MuxUnit::selectLed(const int topLed, const int masLed, const int rasLed)
{
    sprintf(_cCmd, SELECT_LED, topLed, masLed, rasLed);
    if (this->issueCommand(_cCmd) == false)
        return false;

    std::string sLine;
    if (this->receiveLine(sLine) == false)
        return false;

    int     current;
    bool    result = true;
    int     n      = sscanf(sLine.c_str(), "LED:%d", &current);

    if (n != 1 || (current < MIN_LED_CURRENT && (rasLed+1) != LED_OFF))
    {
        _sLastError = sLine;
        result = false;
    }

    std::string sAckLine;
    (void)this->waitForAck(sAckLine);
    _sLastError += sAckLine;

    return result;
}

/**
 *  \brief  Obtain the firmware version of the TopMux. On error false is returned, true otherwise.
 *  \param  sVersion container and result of the TopMux firmware version
 *  \return bool
 */
bool MuxUnit::getMuxVersion(std::string& sVersion)
{
    sVersion.clear();
    if (this->issueCommand(GET_MUX_VERSION) == false)
        return false;
    return this->waitForAck(sVersion);
}

/**
 *  \brief  Obtain the current applied settings of the TopMux. On error false is returned, true otherwise.
 *  \param  sCurrent container and result of the current applied settings
 *  \return bool
 */
bool MuxUnit::getMuxCurrent(std::string& sCurrent)
{
    sCurrent.clear();
    if (this->issueCommand(GET_MUX_CURRENT) == false)
        return false;
    return this->waitForAck(sCurrent);
}

/**
 *  \brief  Obtain the current applied I2C settings. On error false is returned, true otherwise.
 *  \param  i2cset container and result of the current applies I2C settings
 *  \return bool
 */
bool MuxUnit::getI2cSetting(std::string& i2cset)
{
    i2cset.clear();
    if (this->issueCommand(GET_I2C_SETTING) == false)
        return false;
    return this->waitForAck(i2cset);
}

/**
 *  \brief  Write the \e setup1 register of the I2C-set to the TopMux.
 *  The parameters are encoded into #_cCmd variable, which is passed as argument to the #handleI2CCommand
 *  method. The result of this method is returned. The meaning of the parameters is not explained.
 *  \param  normalbacklit
 *  \param  linearmode
 *  \param  autogain
 *  \param  inhibitblack
 *  \param  enableauto
 *  \param  horshuffle
 *  \param  vershuffle
 *  \param  forceblack
 *  \param  div0
 *  \param  div1
 *  \return bool
 */
bool MuxUnit::wrI2cSetup1(const bool normalbacklit, const bool linearmode, const bool autogain,
                          const bool inhibitblack,  const bool enableauto, const bool horshuffle,
                          const bool vershuffle,    const bool forceblack, const bool div0, const bool div1)
{
    unsigned short  reg = 0;

    if (normalbacklit == true)
        reg |= BIT_0;
    if (linearmode == true)
        reg |= BIT_1;
    if (autogain == true)
        reg |= BIT_2;
    if (inhibitblack == true)
        reg |= BIT_3;
    if (enableauto == true)
        reg |= BIT_4;
    if (horshuffle == true)
        reg |= BIT_5;
    if (vershuffle == true)
        reg |= BIT_6;
    if (forceblack == true)
        reg |= BIT_7;
    if (div0 == true)
        reg |= BIT_8;
    if (div1 == true)
        reg |= BIT_9;
    reg |= BIT_10;      // internal register


    sprintf(_cCmd, I2C_SETUP1, reg);

    return this->handleI2CCommand(_cCmd);
}

/**
 *  \brief  Write the \e setup2 register of the I2C-set to the TopMux.
 *  The parameters are encoded into #_cCmd variable, which is passed as argument to the #handleI2CCommand
 *  method. The result of this method is returned. The meaning of the parameters is not explained.
 *  \param  readmode
 *  \param  pixelmode
 *  \param  pixthreshold
 *  \param  shufflemode
 *  \return bool
 */
bool MuxUnit::wrI2cSetup2(const bool readmode, const int pixelmode, const bool pixthreshold, const int shufflemode)
{
    unsigned short  reg = 0;

    if (readmode == true)
        reg |= BIT_0;   // Read mode A
    else
        reg |= BIT_1;   // Read mode B

    unsigned short  pixel = ((unsigned short)(pixelmode << 2) & 0x3c);      // bit 2-5
    reg |= pixel;

    if (pixthreshold == true)
        reg |= BIT_6;

    unsigned short  shuffle = ((unsigned short)(shufflemode << 9) & 0xe00); // bit 9-11
    reg |= shuffle;

    sprintf(_cCmd, I2C_SETUP2, reg);

    return this->handleI2CCommand(_cCmd);
}

/**
 *  \brief  Write the \e setup3 register of the I2C-set to the TopMux.
 *  The parameters are encoded into #_cCmd variable, which is passed as argument to the #handleI2CCommand
 *  method. The result of this method is returned. The meaning of the parameters is not explained.
 *  \param  pixeloffset
 *  \param  enablesno
 *  \return bool
 */
bool MuxUnit::wrI2cSetup3(const int pixeloffset, const bool enablesno)
{
    unsigned short  reg = 0;

    unsigned short  offset = (unsigned short)(pixeloffset & 0x3f);  // bit 0-5
    reg |= offset;

    if (enablesno == true)
        reg |= BIT_6;

    sprintf(_cCmd, I2C_SETUP3, reg);

    return this->handleI2CCommand(_cCmd);
}

/**
 *  \brief  Write the \e analog register of the I2C-set to the TopMux.
 *  The parameters are encoded into #_cCmd variable, which is passed as argument to the #handleI2CCommand
 *  method. The result of this method is returned. The meaning of the parameters is not explained.
 *  \param  antiblooming
 *  \param  blackreference
 *  \param  whitethreshold
 *  \param  binairisation
 *  \return bool
 */
bool MuxUnit::wrI2cAnalog(const bool antiblooming,   const bool blackreference,
                          const bool whitethreshold, const bool binairisation)
{
    unsigned short  reg = 0;

    reg |= BIT_0;   // mandatory
    if (antiblooming == true)
        reg |= BIT_3;
    if (blackreference == true)
        reg |= BIT_6;
    if (whitethreshold == true)
        reg |= BIT_7;
    if (binairisation == true)
        reg |= BIT_9;
    
    sprintf(_cCmd, I2C_ANALOGUE, reg);

    return this->handleI2CCommand(_cCmd);
}

/**
 *  \brief  Write the \e gain register of the I2C-set to the TopMux.
 *  The parameters are encoded into #_cCmd variable, which is passed as argument to the #handleI2CCommand
 *  method. The result of this method is returned.
 *  \param  gain
 *  \return bool
 */
bool MuxUnit::wrI2cGain(const int gain)
{
    unsigned short  reg = (unsigned short)gain & 0xf;       // bit 0-3

    sprintf(_cCmd, I2C_GAIN, reg);

    return this->handleI2CCommand(_cCmd);
}

/**
 *  \brief  Write the \e coarse register of the I2C-set to the TopMux.
 *  The parameters are encoded into #_cCmd variable, which is passed as argument to the #handleI2CCommand
 *  method. The result of this method is returned.
 *  \param  coarse
 *  \return bool
 */
bool MuxUnit::wrI2cCoarse(const int coarse)
{
    unsigned short  reg = (unsigned short)coarse & 0x1ff;   // bit 0-8

    sprintf(_cCmd, I2C_COARSE, reg);

    return this->handleI2CCommand(_cCmd);
}

/**
 *  \brief  Write the \e fine register of the I2C-set to the TopMux.
 *  The parameters are encoded into #_cCmd variable, which is passed as argument to the #handleI2CCommand
 *  method. The result of this method is returned.
 *  \param  fine
 *  \return bool
 */
bool MuxUnit::wrI2cFine(const int fine)
{
    unsigned short  reg = (unsigned short)fine & 0x1ff; // bit 0-8

    sprintf(_cCmd, I2C_FINE, reg);

    return this->handleI2CCommand(_cCmd);
}

/**
 *  \brief  Write the \e lower register of the I2C-set to the TopMux.
 *  The parameters are encoded into #_cCmd variable, which is passed as argument to the #handleI2CCommand
 *  method. The result of this method is returned.
 *  \param  lower
 *  \return bool
 */
bool MuxUnit::wrI2cLower(const int lower)
{
    unsigned short  reg = (unsigned short)lower & 0x1ff;    // bit 0-8

    sprintf(_cCmd, I2C_LOWER, reg);

    return this->handleI2CCommand(_cCmd);
}

/**
 *  \brief  Write the \e upper register of the I2C-set to the TopMux.
 *  The parameters are encoded into #_cCmd variable, which is passed as argument to the #handleI2CCommand
 *  method. The result of this method is returned.
 *  \param  upper
 *  \return bool
 */
bool MuxUnit::wrI2cUpper(const int upper)
{
    unsigned short  reg = (unsigned short)upper & 0x1ff;    // bit 0-8

    sprintf(_cCmd, I2C_UPPER, reg);

    return this->handleI2CCommand(_cCmd);
}

/**
 *  \brief  Handle (write) a specific I2C command to the TopMux.
 *  All wrI2C* methods of the #MuxUnit class make use of this routine to write their register
 *  to the TopMux. After sending the command, a result line is read and compared with the string
 *  #I2C_REG_OK. On error false is returned, true otherwise.
 *  \param  sCmd    the I2C write register command
 *  \return bool
 */
bool MuxUnit::handleI2CCommand(const std::string& sCmd)
{
    if (this->issueCommand(sCmd) == false)
        return false;

    std::string sLine;

    for(int i = 0; i < MAX_TRY; ++i)
    {
        if (this->receiveLine(sLine) == false)
            return false;

        if (sLine == I2C_REG_OK)
        {
            std::string sAckLine;
            bool bResult = waitForAck(sAckLine);
            _sLastError += sAckLine;
            return bResult;
        }
    }
    _sLastError = "HandleI2CCommand failed";
    return false;
}
