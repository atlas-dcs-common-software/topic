/**
 *  \file   httpAnal.cpp
 *  \brief  Interface code between Rasdim and the analysis servers.
 *
 *  It uses the \e http protocol built upon the \e libcurl library. The interface code is written
 *  as a set of C-routines.
 *  Original source by Bram Bouwens.
 *  Soap is an analysis application (written in Java) for the Rasnik type of channels.
 *  Spot is an analysis application (written in C++) for the Spot type of channels.
 *
 *  \author Robert.Hart@nikhef.nl
 *  \date   May 2014
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "curl/curl.h"
#include "interface.h"

#define URL_SIZE    1024    /**< Buffer size of the URL message */

#define C_COMMA     ','     /**< The comma character */
#define C_DOT       '.'     /**< The dot character */

static  BYTE    *pBuffer = NULL;
static  BYTE    *inPointer;

/**
 *  \fn     static size_t write_data(void *ptr, size_t size, size_t nmemb, void *notused)
 *  \brief  Call-back function to copy the result into the buffer.
 *  \param  ptr pointer to the result
 *  \param  size element size
 *  \param  nmemb number of elements
 *  \param  notused what it says
 *  \return totalSize what it says
 */
static size_t write_data(void *ptr, size_t size, size_t nmemb, void *notused)
{
    notused = notused;  // fools the compiler
    size_t totalSize = size * nmemb;
    memcpy(inPointer, ptr, totalSize);
    inPointer += totalSize;
    return totalSize;
}

/**
 *  \fn     bool httpIsup(const char* urlHost, const int urlPort)
 *  \brief  Check if Soap server is running
 *  \param  urlHost host name or IP number of machine Soap-server is running on
 *  \param  urlPort port number Soap-server is using
 *  \return True if Soap-server is running, false otherwise.
 */
bool httpIsup(const char* urlHost, const int urlPort)
{
    CURL*   pCurl = curl_easy_init();
    if (pCurl == NULL)
        return false;

    char    httpUrl[URL_SIZE];
    sprintf(httpUrl, "http://%s:%d/isup", urlHost, urlPort);

    pBuffer = (unsigned char *)malloc(URL_SIZE);

    curl_easy_setopt(pCurl, CURLOPT_WRITEFUNCTION, &write_data);
    curl_easy_setopt(pCurl, CURLOPT_URL,           httpUrl);

    inPointer = pBuffer;
    CURLcode res = curl_easy_perform(pCurl);
    if (res != CURLE_OK)
    {
        free(pBuffer);
        curl_easy_cleanup(pCurl);
        return false;
    }

    // now the inPointer points to the end of the input, drop in an end-of-string 0
    *inPointer = 0;
    // pCurl is not necessary anymore
    curl_easy_cleanup(pCurl);

    bool bResult = true;
    if (strncmp((const char*)pBuffer, "yes", 3) != 0)
        bResult = false;
    free(pBuffer);

    return bResult;
}

/**
 *  \fn     bool httpVersion(const char* urlHost, const int urlPort, int *iVersion)
 *  \brief  Get the version of the Soap server.
 *  \param  urlHost host name or IP number of machine Soap-server is running on
 *  \param  urlPort port number Soap-server is using
 *  \param  iVersion pointer to the version number
 *  \return True if version was obtained, false otherwise.
 */
bool httpVersion(const char* urlHost, const int urlPort, int *iVersion)
{
    CURL*   pCurl = curl_easy_init();
    if (pCurl == NULL)
        return false;

    char    httpUrl[URL_SIZE];
    sprintf(httpUrl, "http://%s:%d/version", urlHost, urlPort);

    pBuffer = (unsigned char *)malloc(URL_SIZE);

    curl_easy_setopt(pCurl, CURLOPT_WRITEFUNCTION, &write_data);
    curl_easy_setopt(pCurl, CURLOPT_URL,           httpUrl);

    inPointer = pBuffer;
    CURLcode res = curl_easy_perform(pCurl);
    if (res != CURLE_OK)
    {
        free(pBuffer);
        curl_easy_cleanup(pCurl);
        return false;
    }

    // now the inPointer points to the end of the input, drop in an end-of-string 0
    *inPointer = 0;
    // pCurl is not necessary anymore
    curl_easy_cleanup(pCurl);

    sscanf((const char *)pBuffer, "%d", iVersion);

    free(pBuffer);

    return true;
}

/**
 *  \fn     bool httpAnalysis(const char* urlHost, const int urlPort, int* nin, float* fin, BYTE* image, int* iResult, float* dout)
 *  \brief  Call the analysis routine of an analysis server (Soap or Spot).
 *  \param  urlHost host name or IP number of machine analysis server is running on
 *  \param  urlPort port number analysis server is using
 *  \param  nin array with parameters of type \e integer
 *  \param  fin array with parameters of type \e float
 *  \param  image the image to be analyzed
 *  \param  iResult if 0 okay, otherwise image could not be analyzed
 *  \param  dout array with analysis results: x, y, scale, rotz, .....
 *  \return True if function was okay, false otherwise
 */
bool httpAnalysis(const char* urlHost, const int urlPort, int* nin, float* fin, BYTE* image, int* iResult, float* dout)
{
    CURL*   pCurl = curl_easy_init();
    if (pCurl == NULL)
        return false;

    char    httpUrl[URL_SIZE];
    sprintf(httpUrl, "http://%s:%d/analRaw", urlHost, urlPort);
    curl_easy_setopt(pCurl, CURLOPT_URL, httpUrl);

    int     xWidth      = nin[I_X_IMG];
    int     yHeight     = nin[I_Y_IMG];
    int     dRasterSize = xWidth * yHeight;

    int     dNinSize    = MAX_NIN * sizeof(int);
    int     dFinSize    = MAX_FIN * sizeof(float);
    long    lBufferSize = dRasterSize + dNinSize + dFinSize;

    pBuffer = (unsigned char *)malloc(lBufferSize);
    // copy all we need into the buffer
    memcpy(pBuffer, image, dRasterSize);
    memcpy(pBuffer + dRasterSize, nin, dNinSize);
    memcpy(pBuffer + dRasterSize + dNinSize, fin, dFinSize);

    curl_easy_setopt(pCurl, CURLOPT_POSTFIELDS,    pBuffer);
    curl_easy_setopt(pCurl, CURLOPT_POSTFIELDSIZE, lBufferSize);
    curl_easy_setopt(pCurl, CURLOPT_NOPROGRESS,    1);
    curl_easy_setopt(pCurl, CURLOPT_WRITEFUNCTION, &write_data);

    // next 4 lines added by PFG in order to use Spot analysis
    struct curl_slist *list = NULL;
    list = curl_slist_append(list, "Content-Type: application/octet-stream");
    list = curl_slist_append(list, "Expect:"); // disables the Expect header
    curl_easy_setopt(pCurl, CURLOPT_HTTPHEADER, list);

    inPointer = pBuffer;
    CURLcode res = curl_easy_perform(pCurl);

    curl_slist_free_all(list); // frees the list

    if (res != CURLE_OK)
    {
        free(pBuffer);
        curl_easy_cleanup(pCurl);
        return false;
    }

    // now the inPointer points to the end of the input, drop in an end-of-string 0
    *inPointer = 0;
    // pCurl is not necessary anymore
    curl_easy_cleanup(pCurl);

    sscanf((const char *)pBuffer, "%x", iResult);
    if (*iResult != 0)
    {
        free(pBuffer);
        return true;    // analysis failed, but the function did not
    }

    // The analysis (Java in particular) scripts could print the floats in the localized format.
    // If the PC is set to for instance Dutch, the floats contain comma's.
    // So, we replace all comma's by dots.
    unsigned int    dBuflen = (unsigned int)strlen((const char*)pBuffer);
    for (unsigned int i = 0; i < dBuflen; ++i)
    {
        if (pBuffer[i] == C_COMMA)
            pBuffer[i] = C_DOT;
    }

    // the results are in the buffer in a simple text format:
    int n = sscanf((const char *)pBuffer, "%*x %f %f %f %f %f %f %f %f",
                    &dout[1], &dout[2], &dout[3], &dout[4], &dout[5], &dout[6], &dout[7], &dout[8]);
    free(pBuffer);

    if (n != 8)
        return false;

    return true;
}
