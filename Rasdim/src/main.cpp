/**
 *  \file   main.cpp
 *  \author Robert.Hart@nikhef.nl
 *  \date   June 2016
 *  \brief  Starting point of the \e Rasdim server by creating an instance of the #Server class and
 *          calling the #Server.runServer() method.
 */

/**
 *  \mainpage [Rasdim]
 *  \author Robert.Hart@nikhef.nl
 *  \date   March 2018
 *
 *  \section intro_sec Introduction
 *  The \e Rasdim server takes care of handling the barrel alignment channels of ATLAS.
 *  Version 8 and higher run on an embedded dual core ARM Cortex A9 processor with a Linux 4.6.0 kernel,
 *  which is mounted on a board manufactured by \e Topic \e Embedded \e Products including a
 *  Xilinx Zynq Z7015 FPGA and standard peripherals like GBit ethernet and USB. The board is connected
 *  by 2 cables to a so-called TopMux (a multiplexer on top of the multiplexer hierarchy).
 *
 *  -# DVI cable for the video signal, which is captured and digitized by the ADC of the Xilinx FPGA.
 *  -# COMport to control the TopMux, like selecting camera and light source, reset the TopMux, etc.
 *
 *  The introduction of the board in 2016 (aka <em> Ethernet based frame grabber </em>) is part of a
 *  Nikhef project, called: <b>Rejuvenation of the ATLAS barrel alignment controls</b>.
 *  Detailed information can be found on the Nikhef project management web server:
 *  https://redmine.nikhef.nl/et/projects/atlas_maintenance_rasnik
 *
 *  \section func_sec Usage
 *  A typical select/analyze channel request triggers the server to perform the following steps:
 *  -# Switch on the camera and light source of the channel (optionally any I2C setting as well)
 *  -# Grab the image (by means of the ADC)
 *  -# Analyze the image and send the results back to the client
 *
 *  \section dep_sec Dependencies
 *  The Rasdim server is dependent on the following external packages: <br>
 *  -# The Rasdim server communicates with its clients by means of \e DIM (http://dim.web.cern.ch/dim/), a communication package provided by CERN.
 *  -# The ADC of the FPGA is controlled and monitored by the \e libiio package of Analog Devices (http://analogdevicesinc.github.io/libiio).
 *  -# There are 2 types of analysis (modules) supported: \e Soap and \e Spot. Each type is available as server and the
 *     communication between the analysis servers and Rasdim is based on the \e libcurl package.
 *  -# The \e libconfigfile package is used to utilize a configuration file, hence \c -lconfigfile.
 *
 *  \section impl_sec Implementation Details
 *  The Xilinx SDK is used to (cross) compile and link the Rasdim server.
 *  Execute the following commands to use this SDK: <br>
 *  \code
 *  [prompt] source /eda/fpga/xilinx/SDK/2016.4/settings64.sh
 *  [prompt] xsdk -vmargs -Dorg.eclipse.swt.internal.gtk.cairoGraphics="false"
 *  \endcode
 *  In order to use this SDK make sure to add the following line in your \c .bashrc file:
 *  \code
 *  export LM_LICENSE_FILE=@loue.nikhef.nl
 *  \endcode
 *  The \e svn repository is located at: \c /project/ct/po/svn/Rasdim <br>
 *  The source code is located at: \c /project/ct/po/workspace/Topic <br>
 *  Not only the Rasdim sources are located here, but also the source of DIM (i.e. \c dimc and \c dimcxx)
 *  and the \c configfile package. The Rasdim sources are compiled with the following command:
 *  \code
 *  arm-xilinx-linux-gnueabi-g++
 *  \endcode
 *  and following arguments:
 *  \code
 *  -Wall -O0 -g3 -I../include -I../../dim/include -I../../include -c -fmessage-length=0 -fPIC -MT"$@" -std=c++11
 *  \endcode
 *  To link and load the server the same command is used with the following linker folders and libraries:
 *  \code
 *  -L../../dim/lib -L../../lib
 *  -ldimcxx -ldimc -lconfigfile -lpthread -lcurl -lgnutls -lnettle -lhogweed -lgmp -lz -liio -lusb-1.0 -lavahi-client -lavahi-common -lxml2 -ldbus-1
 *  \endcode
 *
 *  \section version_sec Version history
 *  The #RD_VERSION constant holds the latest version.
 *  \li 807: reset ADC on timeout, separate enhancing (vs. linear) for rasnik and spot image
 *  \li 808: destroy buffer before throwing exception due to timeout
 *  \li 809: 1. enchant -> enhance; 2. doxygen; 3. beautified
 *  \li 810: 1. more doxygen; 2. -lspot -> -lSpot
 *  \li 811: signal handler
 *  \li 812: Remote SOAP_HOST, no more HOUR_OFFSET
 *  \li 813: Spot analysis like Soap server (http protocol)
 *  \li 814: Race condition detection
 *  \li 815: Permanent RDE_GRABBER (timeout on grab) detection with reboot of Linux system
 *  \li 816: Introduction of RDE_NOANALYSIS if concerned analysis server is not running
 *  \li 817: Using same Mux address does not contribute on permanent RDE_GRABBER detection (V 815)
 *  \li 818: Setting max-transition-width of frame grabber IP block
 *  \li 819: Setting max-transition-width per channel. Misuse of #_NImages variable of class Command
 *  \li 820: Deadlock detection, if blocked commit suicide
 *  \li 821: Remember first channel causing a consecutive list of RDE_GRABBER errors
 *  \li 822: Store first channel of RDE_GRABBER error list into separate log file (Oct 29 2018)
 */

#include <signal.h>
#include "server.h"

bool    bExitNow = false;   /**< Set true if a signal is raised */

void sigHandler(int sigNum)
{
    bExitNow = true;
}

int main( int argc, char *argv[] )
{
    Server* _pServer = new Server();

    if (_pServer->getStatus() == false)
        return -1;

    signal(SIGHUP,  SIG_IGN);
    signal(SIGINT,  sigHandler);
    signal(SIGQUIT, sigHandler);
    signal(SIGTERM, sigHandler);

    _pServer->runServer();

    delete _pServer;

    return 0;
}
