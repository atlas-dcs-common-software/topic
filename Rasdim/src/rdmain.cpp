/**
 *  \file   rdmain.cpp
 *  \brief  Implementation of the RdMain class.
 *  \author Robert.Hart@nikhef.nl
 *  \date   May 2014
 */
#include <iostream>
#include <stdio.h>
#include <time.h>
#include "rdmain.h"
#include "server.h"
#include "videograbber.h"
#include "muxunit.h"
#include "tiff.h"
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

#define CONS_GRAB_ERRORS    8           /**< Maximum number of consecutive grab errors */

bool            bHandleRequest = false;	/**< True if request is being handled, false otherwise */
unsigned  int   uRequests      = 0;     /**< Number of requests since startup */

extern  bool    bExitNow;

// to use httpAnal.cpp
extern  bool    httpIsup    (const char* urlHost, const int urlPort);
extern  bool    httpVersion (const char* urlHost, const int urlPort, int *iVersion);
extern  bool    httpAnalysis(const char* urlHost, const int urlPort, int* nin, float* fin, BYTE* image, int* iResult, float* dout);


/**
 *  \brief  The (only) constructor of the RdMain class.
 *
 *  The RdMain constructor is a kind of DimRpc class and inherits its attributes.
 *  The parameters fmName, fmIn and fmOut are used by the constructor of the DimRpc class.
 *  The other parameters, mainly pointers to other instances, are copied into protected attributes.
 *  Further important actions are:
 *  \li allocate enough memory for the image to be analyzed (#_pAnalImg)
 *  \li make sure the Soap-server is running and get its version number
 *  \li check if Spot-server is running and get its version number
 *  \li expose the first (known as \e epoch) image of the #VideoGrabber instance
 *
 *  \param  pServer pointer to an object (the caller) of the Server class
 *  \param  pVideoGrabber pointer to an instance of the VideoGrabber class
 *  \param  pMuxUnit pointer to an instance of the MuxUnit class
 *  \param  fmName name of this server
 *  \param  fmIn format of the request (class Command)
 *  \param  fmOut format of the reply (class Result)
 *
 *  \return void
 */
RdMain::RdMain(Server* pServer, VideoGrabber* pVideoGrabber, MuxUnit* pMuxUnit,
           const char* fmName, const char* fmIn, const char* fmOut)
       : DimRpc(fmName, fmIn, fmOut)
{
    _pServer       = pServer;
    _pVideoGrabber = pVideoGrabber;
    _pMuxUnit      = pMuxUnit;

    _pCommand      = new Command;
    _pResult       = new Result;
    _pAnalImg      = new BYTE[MAX_X_IMG * MAX_Y_IMG];
    _bSavedImage   = false;
    _uConsGrabErr  = 0;
    _sFirstGrabErr = "_NONE_";

    _bSameAddr     = false;
    _bUseI2c       = false;

    _bSoapState    = false;
    _sSoapHost     = _pServer->getLocalSoapHost();
    _iSoapVersion  = -1;
    _bSpotState    = false;
    _sSpotHost     = _pServer->getSpotHost();
    _iSpotVersion  = -1;

    if (checkSoapServer() == false)
    {
        // start the Soap server locally
        sleep(1);
        (void)startSoapServer();
    }
    sprintf(_cBuf, "- Soap analysis version: %5d (x%X)", _iSoapVersion, _iSoapVersion);
    dualLog(_cBuf);

    if (checkSpotServer() == true)
    {
        sprintf(_cBuf, "- Spot analysis version: %5d (x%X)", _iSpotVersion, _iSpotVersion);
        dualLog(_cBuf);
    }
    sleep(1);

    blackBorder();
    exposeImage("epoch", ATLAS_X_IMG, ATLAS_Y_IMG, _pAnalImg);
}

/**
 *  \brief  Destructor of the RdMain class.
 *          It resets the TopMux (switches it off) and frees the image buffer #_pAnalImg.
 *  \warning    It should call the destructor of some other instances as well.
 *  \return void
 */
RdMain::~RdMain()
{
    mxResetMux();
    delete [] _pAnalImg;
}

/**
 *  \brief  The request of the clients come here, they are handled and the replies are send back.
 *
 *  This method is defined as \b virtual \b void by the class DimRpc and hence it has to be implemented
 *  by the RdMain class. The request is read by the \e getData() method of DimRpc and \e setData() of
 *  the same class is used to send the reply back.
 *
 *  \return void
 */
void RdMain::rpcHandler()
{
    if (bExitNow == true)
        return;

    bHandleRequest = true;
    ++uRequests;                                // another request
    _pCommand = (Command*)getData();            // get request
    time(&_tClock);                             // time of arrival
    preHandleCommand();                         // pre-init reply
    handleRequest();                            // decode and execute request
    setData(_pResult, sizeof(class Result));    // reply to caller
    postHandleCommand();                        // clean up request
    bHandleRequest = false;
}

/**
 *  \brief  Decode the incoming request.
 *  \return void
 */
void RdMain::handleRequest()
{
    switch(_pCommand->_Cmd)
    {
    case CMD_PROBE:
        break;
    case CMD_GRAB:
    case CMD_ANAL:      // common usage
        handleGrab();
        break;
    case CMD_MUX_OFF:
        handleResetMux();
        break;
    case CMD_VIEW:      // not supported anymore
    case CMD_LED_ON:    // not supported anymore
    case CMD_LED_OFF:   // not supported anymore
    case CMD_QUIT:      // not supported anymore
    default:
        _pResult->_Result = RDE_ILLCMD;
        sprintf(_cBuf, "unknown command x%X", _pCommand->_Cmd);
        dualLog(_cBuf);
        break;
    }
}

/**
 *  \brief  The request requires an image of a channel.
 *
 *  The following steps are carried out:
 *  \li The camera and led of the channel are selected, together with some optional I2C settings.
 *      The max-trans-width capture variable is adjusted as set by the channel. For this purpose the
 *      _NImages field of the Command class is used. This field was not used anymore.
 *      These steps are skipped if the channel address is the same as the previous one, hence the camera and
 *      led address are the same.
 *  \li The image is taken and placed into #_pAnalImg. If an I2C setting was applied, the I2C delay
 *      parameter is used, if not the common delay is applied.
 *  \li The grabbed images is exposed by the #Server::_pImageService DimService.
 *  \li If wanted or needed the image is saved as tiff file.
 *  \li If the request requires analysis, the method #analyseImage is called.
 *
 *  \return void
 */
void RdMain::handleGrab()
{
    int         dServer   = _pServer->getServer();
    int         dAnalType = _pCommand->_Param._AnalType;
    MuxAddress* pMux      = &_pCommand->_MuxAddress;
    bool        bEnhance  = false;

    if (pMux->_CamServer != dServer || pMux->_LedServer != dServer)
    {
        _pResult->_Result = RDE_WRONGSERVER;
        return;
    }

    switch (dAnalType)
    {
        case T_SEFO:
        case T_SOAP:
            bEnhance = _pServer->getRasnikEnhance();
            break;
        case T_SPOT:
            bEnhance = _pServer->getSpotEnhance();
            break;
        default:
            bEnhance = false;
            break;
    }

    // Set the version, width and height parameters inside the Nin array.
    // The caller does not know these parameters.
    _pCommand->_Param._Nin[I_VERSION] = 0;
    _pCommand->_Param._Nin[I_X_IMG]   = ATLAS_X_IMG;
    _pCommand->_Param._Nin[I_Y_IMG]   = ATLAS_Y_IMG;

    _bSameAddr = this->isSameChannel();
    _bUseI2c   = this->isI2cChannel(_pCommand->_I2c);

    if (_bSameAddr == false)
    {
        /* FAKE channel
        if (pMux->_CamRasMux == 4)
        {
            dualLog("FAKE");
            (void)this->mxSwitchOff();
        }
        else
        {*/

        if (mxSwitchCam(pMux->_CamTopMux, pMux->_CamMasterMux, pMux->_CamRasMux) == false)
        {
            _pResult->_Result = RDE_MUX;
            return;
        }
        if (mxHandleI2c(_pCommand->_I2c) == false)
        {
            _pResult->_Result = RDE_MUX;
            return;
        }
        if (mxSwitchLed(pMux->_LedTopMux, pMux->_LedMasterMux, pMux->_LedRasMux) == false)
        {
            _pResult->_Result = RDE_MUX;
            return;
        }

        /*}*/
        unsigned int  uTransWidth;
        if (_pCommand->_NImages <= 0)
        	uTransWidth = _pServer->getDefTransWidth();
        else
        	uTransWidth = (unsigned int)_pCommand->_NImages;
        if (uTransWidth != _pServer->getCurTransWidth())
        	_pServer->setTransWidth(uTransWidth);
    }

    unsigned long   uGrabDelay;
    if (_bSameAddr == true || _bUseI2c == true)
        uGrabDelay = _pServer->getI2cGrabDelay();
    else
        uGrabDelay = _pServer->getGrabDelay();

    // All set, let's grab.
    try
    {
        _pVideoGrabber->grab(uGrabDelay, bEnhance);
    }
    catch(IIOException ex)
    {
        sprintf(_cBuf, "ERROR: %s", ex.what());
        dualLog(_cBuf);
        _pResult->_Result = RDE_GRABBER;
        return;
    }

    blackBorder();
    exposeImage(_pCommand->_Channel, ATLAS_X_IMG, ATLAS_Y_IMG, _pAnalImg);

    if ((_pCommand->_AuxCmd & AUX_SAVE_IMAGE) != 0)
        handleSaveImage();  // save the image
    
    if (_pCommand->_Cmd != CMD_ANAL)
        return;

    // There is an image to be analyzed!
    _bAnalServer = true;  // assumption
    if (analyseImage(_pAnalImg) == false)
    {
        if (_bAnalServer == false)
            _pResult->_Result = RDE_NOANALYSIS;
        else
        {
            _pResult->_Result = RDE_ANALYSIS;
            // Error on analysis. Save image, but first check if caller wants to.
            if ((_pCommand->_AuxCmd & AUX_DONT_SAVE_BADIMAGE) == 0)
                handleSaveImage();
        }
    }
}

/**
 *  \brief  Call #mxResetMux to reset the TopMux.
 *  \return void
 */
void RdMain::handleResetMux()
{
    if (mxResetMux() == false)
        _pResult->_Result = RDE_MUX;
}

/**
 *  \brief  Analyze the grabbed image.
 *
 *  The following actions are performed:
 *  \li Decode which analysis type we need to use: \e Soap or \e Spot.
 *  \li If Soap call #analSoapImage, if Spot call #analSpotImage.
 *  \li Collect the results and prepare the output and Result instance.
 *
 *  Return true if okay, false if something went wrong or the analysis failed.
 *
 *  \param  pImage pointer to the image.
 *  \return bool
 */
bool RdMain::analyseImage(BYTE* pImage)
{
    int     iAnalVersion = 0;
    bool    bResult      = false;
    int     dAnalType    = _pCommand->_Param._AnalType;

    _pResult->_AnalResult = -1;     // in case analysis type is not supported

    switch (dAnalType)
    {
    case T_SEFO:
    case T_SOAP:
        bResult = analSoapImage(pImage);
        iAnalVersion = _iSoapVersion;
        break;
    case T_SPOT:
        bResult = analSpotImage(pImage);
        iAnalVersion = _iSpotVersion;
        break;
    case T_RASNIK:
    case T_FOAM:
    case T_DOAP:
        sprintf(_cBuf, "Analysis type [%s] is not supported", analType2String(dAnalType).c_str());
        dualLog(_cBuf);
        return false;
    default:
        sprintf(_cBuf, "Unknown analysis type x%x", dAnalType);
        dualLog(_cBuf);
        return false;
    }

    struct tm*  timeinfo = localtime(&_tClock);
    sprintf(_cBuf, "rd_%d%02d%02d.txt", timeinfo->tm_year+1900, timeinfo->tm_mon+1, timeinfo->tm_mday);
    std::string sFileName   = _cBuf;
    std::string sResultFile = _pServer->getResultDir() + "/" + sFileName;

    FILE    *fp;
    fp = fopen(sResultFile.c_str(), "a");
    if (fp == NULL)
    {
        sprintf(_cBuf, "Cannot write result file: %s", sResultFile.c_str());
        dualLog(_cBuf);
        // don't overwrite an earlier error
        if (_pResult->_Result == NO_ERROR)
            _pResult->_Result = RDE_OUTPUT;
        return false;
    }

    // create and write the 'st' line
    MuxAddress* pMux = &_pCommand->_MuxAddress;
    fprintf(fp, "st,%6d,%d/%02d/%02d,%02d:%02d:%02d,%s,%d.%d.%d.%d.%d.%d.%d.%d,%04d,%04d\r\n",
        _pCommand->_SequenceNr,
        timeinfo->tm_year+1900, timeinfo->tm_mon+1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec,
        _pCommand->_Channel,
        pMux->_LedServer, pMux->_LedTopMux, pMux->_LedMasterMux, pMux->_LedRasMux,
        pMux->_CamServer, pMux->_CamTopMux, pMux->_CamMasterMux, pMux->_CamRasMux,
        _pServer->getServer(),
        iAnalVersion);

    if (bResult == true)
    {
        _pResult->_X        = _fDout[1];
        _pResult->_Y        = _fDout[2];
        _pResult->_Scale    = _fDout[3];
        _pResult->_RotZ     = _fDout[4];
        _pResult->_ErrX     = _fDout[5];
        _pResult->_ErrY     = _fDout[6];
        _pResult->_ErrScale = _fDout[7];
        _pResult->_ErrRotZ  = _fDout[8];
    }
    else
    {
        _pResult->_Result = (_bAnalServer == true ? RDE_ANALYSIS : RDE_NOANALYSIS);
    }
    
    fprintf(fp, "ud %6d %f %f %f %f %f %f %f %f\r\n",
        _pResult->_AnalResult,
        _pResult->_X,
        _pResult->_Y,
        _pResult->_Scale,
        _pResult->_RotZ,
        _pResult->_ErrX,
        _pResult->_ErrY,
        _pResult->_ErrScale,
        _pResult->_ErrRotZ);
    fclose(fp);

    return bResult;
}

/**
 *  \brief  Analyze image using the Soap-server.
 *
 *  Before the httpAnalysis routine is called, make sure the Soap-server is up and running.
 *  If the analysis was successful true is returned, false otherwise.
 *
 *  \param  pImage pointer to the image.
 *  \return bool
 */
bool RdMain::analSoapImage(BYTE* pImage)
{
    if (checkSoapServer() == false)
    {
        // try to restart the local Soap server
        sleep(1);
        if (startSoapServer() == false)
        {
            _bAnalServer = false;
            return false;
        }
    }

    int     iResult;
    bool    bResult = httpAnalysis(_sSoapHost.c_str(),_pServer->getSoapPort(), _pCommand->_Param._Nin, _pCommand->_Param._Fin, pImage, &iResult, _fDout);

    _pResult->_AnalResult = iResult;

    if (bResult == false)
    {
        // Something went wrong. Is Soap still running?
        _bSoapState  = false;
        _bAnalServer = false;
        return false;
    }

    if (iResult != 0)
        bResult = false;

    return bResult;
}

/**
 *  \brief  Check if Soap-server is running. If not, it tries to start a Soap server
 *          (if not running already) locally. The effective server #_sSoapHost is set in this routine.
 *  \return bool: True if server is running (remote or locally), false otherwise.
 */
bool RdMain::checkSoapServer()
{
    if (_bSoapState == true)
        return true;

    std::string soapHost  = _pServer->getSoapHost();
    std::string localHost = _pServer->getLocalSoapHost();
    int         soapPort  = _pServer->getSoapPort();

    if (probeSoapServer(soapHost.c_str(), soapPort) == true)
    {
        _sSoapHost  = soapHost;
        _bSoapState = true;
        return true;
    }

    _sSoapHost = localHost;
    if (soapHost == localHost)
        return false;
    if (probeSoapServer(localHost.c_str(), soapPort) == false)
        return false;
    return true;
}

/**
 *  \brief  Probe if Soap-server is up and running. It also gets the Soap version number.
 *  \param  soapHost  Name or IP of host on which Soap-server should run
 *  \param  soapPort  Port number of Soap-server
 *  \return bool: True if server is running, false otherwise.
 */
bool RdMain::probeSoapServer(const char* soapHost, const int soapPort)
{
    if (httpIsup(soapHost, soapPort) == true)
    {
        int iVersion;
        if (httpVersion(soapHost, soapPort, &iVersion) == true)
        {
            _iSoapVersion = iVersion;
            return true;
        }
        sprintf(_cBuf, "WARNING: cannot get Soap analysis version on %s:%d", soapHost, soapPort);
        dualLog(_cBuf);
        return false;
    }
    sprintf(_cBuf, "WARNING: Soap analysis not running on %s:%d", soapHost, soapPort);
    dualLog(_cBuf);
    return false;
}

/**
 *  \brief  Start the Soap-server, but only locally!
 *
 *  The necessary parameters, like port number, name and location, are retrieved from the Server instance.
 *  To start the server the \e system call is used.
 *
 *  \return bool: At the end the method #checkSoapServer is called and its value is returned.
 */
bool RdMain::startSoapServer()
{
    std::string sJavaFile;
    std::string sCommand;

    _bSoapState = false;    // for safety, redundant assignment

    sprintf(_cBuf, "%s/%s.jar", _pServer->getSoapDir().c_str(), _pServer->getSoapExec().c_str());
    sJavaFile = _cBuf;

    if (access(sJavaFile.c_str(), F_OK))
    {
        sprintf(_cBuf, "ERROR: soap executable %s does not exist", sJavaFile.c_str());
        dualLog(_cBuf);
        return true;
    }

    sprintf(_cBuf, "java -jar -Dport=%d -DfixedFlip=true %s &", _pServer->getSoapPort(), sJavaFile.c_str());
    sCommand = _cBuf;

    if (system(sCommand.c_str()) < 0)
    {
        sprintf(_cBuf, "ERROR: failed to start soap, %s", sCommand.c_str());
        dualLog(_cBuf);
        return false;
    }

    sprintf(_cBuf, "INFO: soap server started, %s", sCommand.c_str());
    dualLog(_cBuf);

    sleep(6);
    return checkSoapServer();
}

/**
 *  \brief  Analyze image using the Spot-server.
 *  \param  pImage pointer to the image.
 *  \return bool: If the analysis was successful true is returned, false otherwise.
 */
bool RdMain::analSpotImage(BYTE* pImage)
{
    if (checkSpotServer() == false)
    {
        _bAnalServer = false;
        return false;
    }

    int     iResult;
    bool    bResult = httpAnalysis(_sSpotHost.c_str(),_pServer->getSpotPort(), _pCommand->_Param._Nin, _pCommand->_Param._Fin, pImage, &iResult, _fDout);

    _pResult->_AnalResult = iResult;

    if (bResult == false)
    {
        // Something went wrong. Is Spot still running?
        _bSpotState  = false;
        _bAnalServer = false;
        return false;
    }

    if (iResult != 0)
        bResult = false;

    return bResult;
}

/**
 *  \brief  Check if Spot-server is running. The effective server #_sSpotHost is set in this routine.
 *  \return bool: True if server is running, false otherwise.
 */
bool RdMain::checkSpotServer()
{
    if (_bSpotState == true)
        return true;

    std::string spotHost  = _pServer->getSpotHost();
    int         spotPort  = _pServer->getSpotPort();

    if (probeSpotServer(spotHost.c_str(), spotPort) == true)
    {
        _sSpotHost  = spotHost;
        _bSpotState = true;
        return true;
    }
    return false;
}

/**
 *  \brief  Probe if Spot-server is up and running. It also gets the Spot version number.
 *  \param  spotHost  Name or IP of host on which Spot-server should run
 *  \param  spotPort  Port number of Spot-server
 *  \return bool: True if server is running, false otherwise.
 */
bool RdMain::probeSpotServer(const char* spotHost, const int spotPort)
{
    if (httpIsup(spotHost, spotPort) == true)
    {
        int iVersion;
        if (httpVersion(spotHost, spotPort, &iVersion) == true)
        {
            _iSpotVersion = iVersion;
            return true;
        }
        sprintf(_cBuf, "WARNING: cannot get Spot analysis version on %s:%d", spotHost, spotPort);
        dualLog(_cBuf);
        return false;
    }
    sprintf(_cBuf, "WARNING: Spot analysis not running on %s:%d", spotHost, spotPort);
    dualLog(_cBuf);
    return false;
}

/**
 *  \brief  Reset the TopMux.
 *  \return bool: True if okay, false otherwise.
 */
bool RdMain::mxResetMux()
{
    _dLastCamTop = _dLastCamMas = _dLastCamRas = -1;
    _dLastLedTop = _dLastLedMas = _dLastLedRas = -1;

    if (_pMuxUnit->resetMux() == false)
    {
        sprintf(_cBuf, "resetMux failed[%s]", _pMuxUnit->getLastError().c_str());
        dualLog(_cBuf);
        return false;
    }
    return true;
}

/**
 *  \brief  Switch the wanted camera on.
 *  \param  topCam  Output number to MasterMux
 *  \param  masCam  Output number to RasMux
 *  \param  rasCam  Output number to camera
 *  \return bool: True if okay, false otherwise.
 */
bool RdMain::mxSwitchCam(const int topCam, const int masCam, const int rasCam)
{
    // inside the mux's we start at 0!
    int     top = topCam - 1;
    int     mas = masCam - 1;
    int     ras = rasCam - 1;

    if (top < 0 || top >= MAX_TOPMUX || mas < 0 || mas >= MAX_MASTER || ras < 0 || ras > MAX_RASLED)    
    {
        sprintf(_cBuf, "Illegal Camera combination [%d,%d,%d]", topCam, masCam, rasCam);
        dualLog(_cBuf);
        _dLastCamTop = _dLastCamMas = _dLastCamRas = -1;
        return false;
    }

    if (_pMuxUnit->selectCam(top, mas, ras) == false)
    {
        sprintf(_cBuf, "selectCam(%d,%d,%d) failed [%s]", top, mas, ras, _pMuxUnit->getLastError().c_str());
        dualLog(_cBuf);
        _dLastCamTop = _dLastCamMas = _dLastCamRas = -1;
        return false;
    }

    _dLastCamTop = topCam;
    _dLastCamMas = masCam;
    _dLastCamRas = rasCam;

    return true;
}

/**
 *  \brief  Switch the wanted led on.
 *  \param  topLed  Output number to MasterMux
 *  \param  masLed  Output number to RasMux
 *  \param  rasLed  Output number to camera
 *  \return bool: True if okay, false otherwise.
 */
bool RdMain::mxSwitchLed(const int topLed, const int masLed, const int rasLed)
{
    // inside the mux's we start at 0!
    int     top = topLed - 1;
    int     mas = masLed - 1;
    int     ras = rasLed - 1;

    if (top < 0 || top >= MAX_TOPMUX || mas < 0 || mas >= MAX_MASTER || ras < 0 || ras > MAX_RASLED)    
    {
        sprintf(_cBuf, "Illegal Led combination [%d,%d,%d]", topLed, masLed, rasLed);
        dualLog(_cBuf);
        _dLastLedTop = _dLastLedMas = _dLastLedRas = -1;
        return false;
    }

    if (_pMuxUnit->selectLed(top, mas, ras) == false)
    {
        sprintf(_cBuf, "selectLed(%d,%d,%d) failed [%s]", top, mas, ras, _pMuxUnit->getLastError().c_str());
        dualLog(_cBuf);
        _dLastLedTop = _dLastLedMas = _dLastLedRas = -1;
        return false;
    }

    _dLastLedTop = topLed;
    _dLastLedMas = masLed;
    _dLastLedRas = rasLed;

    return true;
}

/**
 *  \brief  Call #mxExecI2c to apply any I2C setting.
 *  \param  i2c A pointer to an I2c instance.
 *  \return bool: The return value of #mxExecI2c is returned.
 */
bool RdMain::mxHandleI2c(const I2c& i2c)
{
    bool    bResult = mxExecI2c(i2c);

    if (bResult == false)
    {
        sprintf(_cBuf, "HandleI2c failed [%s]", _pMuxUnit->getLastError().c_str());
        dualLog(_cBuf);
    }
    return bResult;
}

/**
 *  \brief  Execute (or apply) the I2C settings of a channel.
 *
 *  The 9 I2C registers are handled one by one, but don't change the order!!!
 *  For instance it is mandatory the setup2 register is treated always first.
 *
 *  \param  i2c A pointer to an I2C instance
 *
 *  \return bool: True if okay, false otherwise.
 */
bool RdMain::mxExecI2c(const I2c& i2c)
{
    /*
     *  NB: Don't change the order of which the I2C registers are handled!!!
     *  For instance it is mandatory the setup2 register is treated always first.
     */

    if (i2c._rSetup2)
    {
        bool    readmode     = (i2c._Readmode     != 0 ? true : false);
        bool    pixthreshold = (i2c._Pixthreshold != 0 ? true : false);

        if (_pMuxUnit->wrI2cSetup2(readmode,
                                    i2c._Pixelmode,
                                    pixthreshold,
                                    i2c._Shufflemode) == false)
            return false;
    }

    if (i2c._rSetup1)
    {
        bool    normalbacklit = (i2c._Normalbacklit != 0 ? true : false);
        bool    linearmode    = (i2c._Linearmode    != 0 ? true : false);
        bool    autogain      = (i2c._Autogain      != 0 ? true : false);
        bool    inhibitblack  = (i2c._Inhibitblack  != 0 ? true : false);
        bool    enableauto    = (i2c._Enableauto    != 0 ? true : false);
        bool    horshuffle    = (i2c._Horshuffle    != 0 ? true : false);
        bool    vershuffle    = (i2c._Vershuffle    != 0 ? true : false);
        bool    forceblack    = (i2c._Forceblack    != 0 ? true : false);
        bool    div0          = (i2c._Div0          != 0 ? true : false);
        bool    div1          = (i2c._Div1          != 0 ? true : false);

        if (_pMuxUnit->wrI2cSetup1(normalbacklit,
                                    linearmode,
                                    autogain,
                                    inhibitblack,
                                    enableauto,
                                    horshuffle,
                                    vershuffle,
                                    forceblack,
                                    div0,
                                    div1) == false)
            return false;
    }

    if (i2c._rSetup3)
    {
        bool    enablesno = (i2c._Enablesno != 0 ? true : false);

        if (_pMuxUnit->wrI2cSetup3(i2c._Pixeloffset, enablesno) == false)
            return false;
    }

    if (i2c._rAnalog)
    {
        bool    antiblooming   = (i2c._Antiblooming   != 0 ? true : false);
        bool    blackreference = (i2c._Blackreference != 0 ? true : false);
        bool    whitethreshold = (i2c._Whitethreshold != 0 ? true : false);
        bool    binairisation  = (i2c._Binairisation  != 0 ? true : false);

        if (_pMuxUnit->wrI2cAnalog(antiblooming,
                                    blackreference,
                                    whitethreshold,
                                    binairisation) == false)
            return false;
    }

    if (i2c._rGain)
    {
        if (_pMuxUnit->wrI2cGain(i2c._Gain) == false)
            return false;
    }

    if (i2c._rCoarse)
    {
        if (_pMuxUnit->wrI2cCoarse(i2c._Coarse) == false)
            return false;
    }

    if (i2c._rFine)
    {
        if (_pMuxUnit->wrI2cFine(i2c._Fine) == false)
            return false;
    }

    if (i2c._rLower)
    {
        if (_pMuxUnit->wrI2cLower(i2c._Lower) == false)
            return false;
    }

    if (i2c._rUpper)
    {
        if (_pMuxUnit->wrI2cUpper(i2c._Upper) == false)
            return false;
    }

    return true;
}

/**
 *  \brief  Check whether the current channel mux-address equals the previous one.
 *  \return bool: True if same channel, false not.
 */
bool RdMain::isSameChannel()
{
    MuxAddress* pMux    = &_pCommand->_MuxAddress;

    if (pMux->_CamTopMux != _dLastCamTop || pMux->_CamMasterMux != _dLastCamMas || pMux->_CamRasMux != _dLastCamRas ||
        pMux->_LedTopMux != _dLastLedTop || pMux->_LedMasterMux != _dLastLedMas || pMux->_LedRasMux != _dLastLedRas)
        return false;
    if (_dLastCamTop < 0 || _dLastCamMas < 0 || _dLastCamRas < 0 ||
        _dLastLedTop < 0 || _dLastLedMas < 0 || _dLastLedRas < 0)
        return false;
    return true;
}

/**
 *  \brief  Check if I2C part of channel requires at least one setting.
 *  \param  i2c A pointer to an I2C instance
 *  \return bool: True if a I2C setting is required, false otherwise.
 */
bool RdMain::isI2cChannel(const I2c& i2c)
{
    if (i2c._rSetup1 || i2c._rSetup2 || i2c._rSetup3 ||
        i2c._rAnalog || i2c._rGain   || i2c._rCoarse ||
        i2c._rFine   || i2c._rLower  || i2c._rUpper)
        return true;
    return false;
}

/**
 *  \brief  Just after receiving a request, do some cleanup, initialization of some
 *          variables and prepare the result where possible.
 *  \return void
 */
void RdMain::preHandleCommand()
{
    _bSavedImage = false;
    _bSameAddr   = false;
    _bUseI2c     = false;

    dualLog(stringRequest());

    int i;
    for(i = 0; i < MAX_CHAN_NAME+1; ++i)
        _pResult->_Channel[i] = '\0';
    for(i = 0; i < MAX_DATE_STR+1; ++i)
        _pResult->_CTime[i] = '\0';

    struct tm*  timeinfo = localtime(&_tClock);
    sprintf(_cBuf, "%02d/%02d/%02d %02d:%02d:%02d",
        timeinfo->tm_year - 100,    // since 1900
        timeinfo->tm_mon + 1,       // 0-11
        timeinfo->tm_mday,
        timeinfo->tm_hour,
        timeinfo->tm_min,
        timeinfo->tm_sec);
    strncpy(_pResult->_CTime, _cBuf, MAX_DATE_STR);
    _pResult->_CTime[MAX_DATE_STR] = '\0';

    strncpy(_pResult->_Channel, _pCommand->_Channel, MAX_CHAN_NAME);
    _pResult->_Channel[MAX_CHAN_NAME] = '\0';

    _pResult->_ChannelId  = _pCommand->_ChannelId;
    _pResult->_SequenceNr = _pCommand->_SequenceNr; 
    _pResult->_Result     = NO_ERROR;       // assumption   
    _pResult->_AnalResult = NO_ERROR;       // assumption
    _pResult->_Time       = _tClock;    
    _pResult->_X          = 0.0;
    _pResult->_Y          = 0.0;
    _pResult->_Scale      = 0.0;
    _pResult->_RotZ       = 0.0;
    _pResult->_ErrX       = 0.0;
    _pResult->_ErrY       = 0.0;
    _pResult->_ErrScale   = 0.0;
    _pResult->_ErrRotZ    = 0.0;
    _pResult->_NSpots     = 0;
    for(i = 0; i < MAX_SPOTS; ++i)
    {
        _pResult->_XSpot[i] = 0.0;
        _pResult->_YSpot[i] = 0.0;
    }

    sprintf(_cBuf, " %-32.32s: ", _pCommand->_Channel);     // main modification of v5.8
    _sPrefix = _cBuf;
    _sStdout = "";
}

/**
 *  \brief  Just after sending the reply, do some cleanup and cosmetics. Check also if system is not in
 *          permanent grabber error state.
 *
 *  Several adjustments, just after the reply, are set and performed.
 *  \li Take care of the #Server::_pWindoutService DimService
 *  \li Create an overall result string for the log file
 *  \li Check if system is in permanent grabber error state. If it does, reboot the system.
 *
 *  \return void
 */
void RdMain::postHandleCommand()
{
    for(int i = 0; i < MAX_WINDOUT; ++i)
        _pServer->_pWindout[i] = '\0';
    int n = (int)_sStdout.size();
    if (n >= MAX_WINDOUT)
        n = MAX_WINDOUT - 1;
    strncpy(_pServer->_pWindout, _sStdout.c_str(), n);
    _pServer->_pWindoutService->updateService(_pServer->_pWindout);

    std::string sResult;

    if (_pResult->_Result == NO_ERROR)
    {
        if (_pCommand->_Cmd == CMD_ANAL)
        {
            sprintf(_cBuf, "[chan: %s] [x: %.5f] [y: %.5f] [scale: %.6f] [rotz: %.6f]",
                _pResult->_Channel, _pResult->_X, _pResult->_Y, _pResult->_Scale, _pResult->_RotZ);
            sResult = _cBuf;
        }
        else
            sResult = "[okay]";
    }
    else
    {
        sprintf(_cBuf, "[chan: %s] [error: ", _pResult->_Channel);
        sResult = _cBuf;

        switch(_pResult->_Result)
        {
        case RDE_ILLCMD:        sResult += "ILLCMD";        break;
        case RDE_MUX:           sResult += "MUX";           break;
        case RDE_GRABBER:       sResult += "GRABBER";       break;
        case RDE_ANALYSIS:      sResult += "ANALYSIS";      break;
        case RDE_OUTPUT:        sResult += "OUTPUT";        break;
        case RDE_WRONGSERVER:   sResult += "WRONSERVER";    break;
        case RDE_ILLBKGRND:     sResult += "ILLBKGRND";     break;
        case RDE_NOSERVER:      sResult += "NOSERVER";      break;
        case RDE_TIMEOUT:       sResult += "TIMEOUT";       break;
        case RDE_INTERNAL:      sResult += "INTERNAL";      break;
        default:
            sprintf(_cBuf, "x%X", _pResult->_Result);
            sResult += _cBuf;
            break;
        }
        if (_pResult->_Result == RDE_ANALYSIS)
        {
            sprintf(_cBuf, ", %d", _pResult->_AnalResult);
            sResult += _cBuf;
        }
        sResult += "]";
    }
    dualLog(sResult);

    if (_pResult->_Result != RDE_GRABBER)
    {
        _uConsGrabErr = 0;
        return;
    }

    // Version 817: using same address does not contribute to consecutive grab errors
    if (_bSameAddr == true)
        return;

    // Version 821: remember first channel causing a list of consecutive grab errors
    if (_uConsGrabErr == 0)
    	_sFirstGrabErr = _pResult->_Channel;

    if (++_uConsGrabErr < CONS_GRAB_ERRORS)
        return;

    // Version 815 ------
    // More than CONS_GRAB_ERRORS of consecutive timeout on grab errors detected.
    // System is now in permanent RDE_GRABBER error mode.
    // Embedded Linux system will be rebooted.
    //
    sprintf(_cBuf, "FATAL: system in permanent RDE_GRABBER state (1st channel: %s), system will be rebooted", _sFirstGrabErr.c_str());
    dualLog(_cBuf);
    _pServer->logGrabErrChannel(_sFirstGrabErr);	// added in Version 822
    (void)_pMuxUnit->resetMux();
    sleep(1);
    std::string sCommand = "shutdown -r now";
    if (system(sCommand.c_str()) < 0)
    {
        _uConsGrabErr = 0;
        sprintf(_cBuf, "ERROR: failed to execute: %s", sCommand.c_str());
        dualLog(_cBuf);
        return;
    }
    sleep(10);  // during this sleep the system will be rebooted
}

/**
 *  \brief  Create and return a brief string of the incoming request.
 *  \return std::string
 */
std::string RdMain::stringRequest()
{
    std::string sRequest;

    sRequest = "[cmd: ";
    switch(_pCommand->_Cmd)
    {
    case CMD_QUIT:      sRequest += "QUIT";     break;
    case CMD_PROBE:     sRequest += "PROBE";    break;
    case CMD_LED_ON:    sRequest += "LED_ON";   break;
    case CMD_LED_OFF:   sRequest += "LED_OFF";  break;
    case CMD_VIEW:      sRequest += "VIEW";     break;
    case CMD_GRAB:      sRequest += "GRAB";     break;
    case CMD_ANAL:      sRequest += "ANAL";     break;
    case CMD_MUX_OFF:   sRequest += "MUX_OFF";  break;
    default:
        sprintf(_cBuf, "x%X", _pCommand->_Cmd);
        sRequest += _cBuf;
        break;
    }
    
    sRequest += "] [aux: ";
    sprintf(_cBuf, "x%X", _pCommand->_AuxCmd);
    sRequest += _cBuf;

    sRequest += "] [anal: ";
    sRequest += analType2String(_pCommand->_Param._AnalType);

    sprintf(_cBuf, "] [chan: %s]", _pCommand->_Channel);
    sRequest += _cBuf;

    return sRequest;
}

/**
 *  \brief  Create and return the analysis type as string.
 *  \param  dAnalType the analysis type
 *  \return std::string
 */
std::string RdMain::analType2String(const int dAnalType)
{
    std::string sAnalType;

    switch(dAnalType)
    {
    case T_RASNIK:  sAnalType = "rasnik"; break;
    case T_SPOT:    sAnalType = "spot";   break;
    case T_FOAM:    sAnalType = "foam";   break;
    case T_SEFO:    sAnalType = "sefo";   break;
    case T_SOAP:    sAnalType = "soap";   break;
    case T_DOAP:    sAnalType = "doap";   break;
    default:
        sprintf(_cBuf, "x%X", dAnalType);
        sAnalType += _cBuf;
        break;
    }
    return sAnalType;
}

/**
 *  \brief  The special log facility of the RdMain class.
 *
 *  It makes use of the public log method of the Server instance.
 *  If bStdout is true or the debug flag of the Server instance is true, the message will also
 *  appear on stdout. This routine also puts its output implicitly onto
 *  the #_sStdout string, which at the end of the request, including the
 *  name of the channel (i.e. #_sPrefix), is exposed by the #Server::_pWindoutService DimService.
 *
 *  \param  sMess the message to be logged
 *
 *  \return void
 */
void RdMain::dualLog(const std::string sMess)
{
    _pServer->log(sMess);

    _sStdout += _sPrefix;
    _sStdout += sMess;
    _sStdout += "\n";
}

/**
 *  \brief  The image needs to be saved
 *
 *  It may be decided at several moments between the request and reply that the image needs to be saved.
 *  Whenever called the following steps are carried out:
 *  \li Check if image (#_bSavedImage) was saved already.
 *  \li Based on getImageDir of the Server instance and time and date create a directory name. If the
 *      directory does not exist, create it.
 *  \li Construct a string sImageFile, made out of the directory name, channel name, date and time.
 *  \li Call the method saveImage with sImageFile as argument and assign its return value to #_bSavedImage.
 *
 *  \return void
 */
void RdMain::handleSaveImage()
{
    if (_bSavedImage == true)
        return;

    struct tm*  timeinfo = localtime(&_tClock);

    sprintf(_cBuf, "%02d%02d%02d", timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
    std::string sTime = _cBuf;
    sprintf(_cBuf, "%04d%02d%02d", timeinfo->tm_year+1900, timeinfo->tm_mon+1, timeinfo->tm_mday);
    std::string sDate = _cBuf;

    std::string sImageDir = _pServer->getImageDir() + "/img" + sDate;

    DIR *pDir;
    pDir = opendir(sImageDir.c_str());
    if (pDir == NULL)
    {
        (void)mkdir((const char*)sImageDir.c_str(), 0775);
        pDir = opendir(sImageDir.c_str());
        if (pDir == NULL)
        {
            sprintf(_cBuf, "Cannot open/create directory %s", sImageDir.c_str());
            dualLog(_cBuf);
            return;
        }
    }
    closedir(pDir);

    std::string sImageFile;
    sImageFile   = sImageDir + "/" + _pCommand->_Channel + "." + sDate + "." + sTime + ".tif";
    _bSavedImage = saveImage(sImageFile);
}

/**
 *  \brief  Save image as tiff file
 *
 *  The tiff file consists of 3 parts:
 *  \li The tiff header, which is created using an instance of the class DCImageHeader.
 *      Important values in this header are the width and height (in pixels) of the image.
 *      This routines uses the constants ATLAS_X_IMG and ATLAS_Y_IMG for width and height.
 *  \li The image itself, i.e. #_pAnalImg.
 *  \li The parameters used for analyzing this image, i.e. an instance of the class Param.
 *
 *  \param  sImageFile  name of the tiff file
 *  \return True if successful, false otherwise.
 */
bool RdMain::saveImage(const std::string sImageFile)
{
    FILE    *fp;
    fp = fopen(sImageFile.c_str(), "w");
    if (fp == NULL)
    {
        sprintf(_cBuf, "Error: cannot create %s", sImageFile.c_str());
        dualLog(_cBuf);
        return false;
    }

    size_t          n, size;
    unsigned int    xWidth  = ATLAS_X_IMG;
    unsigned int    yHeight = ATLAS_Y_IMG;

    DCImageHeader   tifHeader(xWidth, yHeight);

    size = TIFF_HEADER_SIZE;
    n    = fwrite(&tifHeader, size, 1, fp);
    if (n != 1)
    {
        sprintf(_cBuf, "failed to write header to %s", sImageFile.c_str());
        dualLog(_cBuf);
        fclose(fp);
        return false;
    }

    size = xWidth * yHeight;
    n    = fwrite(_pAnalImg, 1, size, fp);
    if (n != size)
    {
        sprintf(_cBuf, "failed to write image to %s", sImageFile.c_str());
        dualLog(_cBuf);
        fclose(fp);
        return false;
    }

    size = sizeof(class Param);
    n = fwrite(&_pCommand->_Param, size, 1, fp);
    if (n != 1)
    {
        sprintf(_cBuf, "Error: param write error (%d, %d)", n , size);
        dualLog(_cBuf);
        fclose(fp);
        return false;
    }

    fclose(fp);
    return true;
}

/**
 *  \brief  Adjust the grabbed image to the barrel alignment ATLAS standard size.
 *
 *  The size of the image grabbed (VV5430_WIDTH * VV5430_HEIGHT) is smaller than the image size
 *  defined and needed for the analysis (ATLAS_X_IMG * ATLAS_Y_IMG). Therefore the image is adjusted
 *  (a little enlarged) by a black border, which are defined by the variables uBlackEdgeX and uBlackEdgeY
 *  as obtained from #_pServer.
 *  The uBlackEdgeX defines the number of black pixels on each line on the left side. The uBlackEdgeY
 *  defines the number of black lines on top. The black pixels on the right side and black lines
 *  at the bottom are easily calculated from these variables above.
 *  The overall result is placed into #_pAnalImg, i.e. the image to be analyzed.
 *
 *  \return void
 */
void RdMain::blackBorder()
{
    unsigned int    x , y;
    unsigned int    xWidth      = _pVideoGrabber->getWidth();   // has to be VV5430_WIDTH
    unsigned int    yHeight     = _pVideoGrabber->getHeight();  // has to be VV5430_HEIGHT
    BYTE*           pData       = (BYTE*)_pVideoGrabber->getData();
    BYTE            cBlack      = 0;
    unsigned int    uBlackEdgeX = _pServer->getBlackEdgeX();
    unsigned int    uBlackEdgeY = _pServer->getBlackEdgeY();
    int             iDest       = 0;
    int             iImg        = 0;
    unsigned int    xTrailer    = ATLAS_X_IMG - xWidth  - uBlackEdgeX;
    unsigned int    yTrailer    = ATLAS_Y_IMG - yHeight - uBlackEdgeY;

    for(y = 0; y < uBlackEdgeY; ++y)
    {
        for(x = 0; x < ATLAS_X_IMG; ++x)
            _pAnalImg[iDest++] = cBlack;
    }
    for(y = 0; y < yHeight; ++y)
    {
        for(x = 0; x < uBlackEdgeX; ++x)
        {
            _pAnalImg[iDest++] = cBlack;
        }
        for(x = 0; x < xWidth; ++x)
        {
            _pAnalImg[iDest++] = pData[iImg++];
        }
        for(x = 0; x < xTrailer; ++x)
        {
            _pAnalImg[iDest++] = cBlack;
        }
    }
    for(y = 0; y < yTrailer; ++y)
    {
        for(x = 0; x < ATLAS_X_IMG; ++x)
            _pAnalImg[iDest++] = cBlack;
    }
}

/**
 *  \brief  Expose/publish the grabbed (and adjusted by blackBorder) image.
 *
 *  The exposition is performed by the DimService #Server::_pImageService.
 *
 *  \param  sChannel name of the channel grabbed
 *  \param  xWidth horizontal pixel size of image
 *  \param  yHeight vertical pixel size of image
 *  \param  pImage pointer to image
 *
 *  \return void
 */
void RdMain::exposeImage(const std::string sChannel, const int xWidth, const int yHeight, BYTE* pImage)
{
    int     dMax    = xWidth * yHeight;

    for(int i = 0; i < dMax; ++i)
        _pServer->_pImage->_Image[i] = pImage[i];
    strncpy(_pServer->_pImage->_Info, sChannel.c_str(), MAX_INFO);
    _pServer->_pImage->_Info[MAX_INFO] = '\0';
    _pServer->_pImage->_Ximg = xWidth;
    _pServer->_pImage->_Yimg = yHeight;
    _pServer->_pImageService->updateService(_pServer->_pImage, sizeof(class Image));
}




