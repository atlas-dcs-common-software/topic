/**
 *  \file   server.cpp
 *  \brief  Implementation of the Server class.
 *  \author Robert.Hart@nikhef.nl
 *  \date   November 2015
 */

#include "server.h"
#include <iostream>
#include <thread>
#include <stdio.h>
#include <errno.h>
#include "configfile.hxx"
#include "interface.h"
#include "rdmain.h"
#include "tiff.h"
#include "videograbber.h"
#include "muxunit.h"
#include <termios.h>

#include <dic.hxx>
#include <dis.hxx>

#define RD_VERSION          822                 /**< latest Rasdim version number */

#define RASDIM_INI_FILE     "./rasdim.ini"      /**< configuration file of the Rasdim server */
#define RASDIM_DUMP_FILE    "./rasdim_dump.ini" /**< file containing the configuration as used */

#define SEC_LOG_FILE        "/var/log/rasdimd"  /**< secundary log-file in case primary could not be written */

#define MAX_REQUEST_LOOP    5                   /**< maximum of waiting for handling a request */

#define TRANS_WIDTH_ADDR    "0x43D00064"        /**< memory address of the max-transition-width parameter */
#define MIN_TRANS_WIDTH     1                   /**< minimum value of max-transition-width */
#define MAX_TRANS_WIDTH     8                   /**< maximum value of max-transition-width */

// configuration parameters
#define VERSION             "Version"
#define SERVER              "Server"
#define DNS_HOST            "DnsHost"
#define GRAB_DELAY          "GrabDelay"
#define I2C_GRAB_DELAY      "I2cGrabDelay"
#define SKIP_FRAMES         "SkipFrames"
#define COMPORT             "ComPort"
#define BAUDRATE            "BaudRate"
#define SOAP_DIR            "SoapDir"
#define SOAP_EXEC           "SoapExec"
#define SOAP_HOST           "SoapHost"
#define SOAP_PORT           "SoapPort"
#define SPOT_HOST           "SpotHost"
#define SPOT_PORT           "SpotPort"
#define LOG_DIR             "LogDir"
#define IMAGE_DIR           "ImageDir"
#define RESULT_DIR          "ResultDir"
#define BLACK_EDGE_X        "BlackEdgeX"
#define BLACK_EDGE_Y        "BlackEdgeY"
#define RASNIK_ENHANCE      "RasnikEnhance"
#define SPOT_ENHANCE        "SpotEnhance"
#define TRANS_WIDTH         "TransWidth"
#define DEBUG               "Debug"
// default configuration parameter values
#define DFLT_SERVER         99
#define DFLT_DNS_HOST       "galego.nikhef.nl"
#define DFLT_GRAB_DELAY     800     // msec
#define DFLT_I2C_GRAB_DELAY 10      // msec
#define DFLT_SKIP_FRAMES    8
#define DFLT_COMPORT        "ttyPS1"
#define DFLT_BAUDRATE       B115200 // bits/sec
#define DFLT_SOAP_DIR       "./soap"
#define DFLT_SOAP_EXEC      "soap-service-1.5"
#define DFLT_SOAP_HOST      "localhost"
#define DFLT_SOAP_PORT      8081
#define DFLT_SPOT_HOST      "localhost"
#define DFLT_SPOT_PORT      8095
#define DFLT_LOG_DIR        "."
#define DFLT_IMAGE_DIR      "./data/images"
#define DFLT_RESULT_DIR     "./data/results"
#define DFLT_BLACK_EDGE_X   5
#define DFLT_BLACK_EDGE_Y   3
#define DFLT_RASNIK_ENHANCE 0
#define DFLT_SPOT_ENHANCE   0
#define DFLT_TRANS_WIDTH    5
#define DFLT_DEBUG          0   // false

extern  bool                bExitNow;
extern  bool                bHandleRequest;
extern  unsigned int        uRequests;

/**
 *  \fn     void _Abs(Server* pServer)
 *  \brief  Thread to determine deadlocks. If detected commit suicide.
 *          Thread is started by an instance of the #Server class.
 *  \param  pServer pointer to the Server object to use its log function
 */
void _Abs(Server* pServer)
{
	pServer->log("Rasdim ABS: started");

	unsigned int   uPrevRequests      = uRequests;
	bool           bPrevHandleRequest = bHandleRequest;

	while(true)
	{
		sleep(60);
		if (bPrevHandleRequest == true && bHandleRequest == true)
		{
			if (uPrevRequests == uRequests)
				break;
		}
		uPrevRequests      = uRequests;
		bPrevHandleRequest = bHandleRequest;
	}
	pServer->log("Rasdim ABS: deadlock detected, commit suicide");
	exit(-1);
}

/**
 *  \brief  The (only) constructor of the Server class.
 *
 *  The Server constructor performs the following actions:
 *  \li Set the boolean #_bStatus to false.
 *  \li Assign the configuration parameters to their default values.
 *  \li (Re)assign the configuration parameters as found in the configuration file #RASDIM_INI_FILE
 *  \li Write the configuration parameters into the file #RASDIM_DUMP_FILE
 *  \li Several redundancy checks concerning types and their sizes.
 *  \li Check the black edge dimensions
 *  \li Create an instance of the class #VideoGrabber. NB: for the width and height arguments the #VV5430_WIDTH
 *      and #VV5430_HEIGHT constants are applied.
 *  \li Create an instance of the class #MuxUnit and get the firmware version of the TopMux.
 *  \li Set the max-transition-width parameter for the frame grabber IP block.
 *  \li Start the #_Abs thread in order to detect deadlocks of the running server.
 *
 *  As soon as one of the actions above fails, the sequence is stopped and the boolean #_bStatus
 *  remains false. If however all actions are successful, then (and only then) #_bStatus
 *  is set to true.
 *
 *  \return void
 */
Server::Server()
{
    _bStatus = false;

    _dServer        = DFLT_SERVER;
    _sDnsNode       = DFLT_DNS_HOST;
    _uGrabDelay     = DFLT_GRAB_DELAY;
    _uI2cGrabDelay  = DFLT_I2C_GRAB_DELAY;
    _uSkipFrames    = DFLT_SKIP_FRAMES;
    _sComPort       = DFLT_COMPORT;
    _dBaudRate      = DFLT_BAUDRATE;
    _sSoapDir       = DFLT_SOAP_DIR;
    _sSoapExec      = DFLT_SOAP_EXEC;
    _sSoapHost      = DFLT_SOAP_HOST;
    _sLocalSoapHost = DFLT_SOAP_HOST;
    _dSoapPort      = DFLT_SOAP_PORT;
    _sSpotHost      = DFLT_SPOT_HOST;
    _dSpotPort      = DFLT_SPOT_PORT;
    _sLogDir        = DFLT_LOG_DIR;
    _sImageDir      = DFLT_IMAGE_DIR;
    _sResultDir     = DFLT_RESULT_DIR;
    _uBlackEdgeX    = DFLT_BLACK_EDGE_X;
    _uBlackEdgeY    = DFLT_BLACK_EDGE_Y;
    _bRasnikEnhance = false;
    _bSpotEnhance   = false;
    _uCurTransWidth = 0;
    _uDefTransWidth = DFLT_TRANS_WIDTH;
    _bDebug         = false;
    sprintf(_cBuf, "%d.%d.%d", RD_VERSION/100, (RD_VERSION%100)/10, RD_VERSION%10);
    _sRdVersion     = _cBuf;

    // get the configuration parameters from the ini-file
    getConfigParams(RASDIM_INI_FILE);
    // first log message
    sprintf(_cBuf, "Rasdim %s started", _sRdVersion.c_str());
    log(_cBuf);
    // dump the configuration parameters into the dump-file
    dumpConfigParams(RASDIM_DUMP_FILE);

    // redundancy checks
    int     intSize   = sizeof(int);
    int     shortSize = sizeof(short);
    int     floatSize = sizeof(float);
    int     byteSize  = sizeof(BYTE);
    int     tiffSize  = sizeof(class DCImageHeader);
    if (intSize != 4 || shortSize != 2 || floatSize != 4 || byteSize != 1)
    {
        sprintf(_cBuf, "FATAL: sizeof(int) = %d, sizeof(short) = %d, sizeof(float) = %d, sizeof(BYTE) = %d", intSize, shortSize, floatSize, byteSize);
        log(_cBuf);
        return;
    }
    if (tiffSize != TIFF_HEADER_SIZE)
    {
        sprintf(_cBuf, "FATAL: sizeof(TIFF_HEADER) = %d, must be %d", tiffSize, TIFF_HEADER_SIZE);
        log(_cBuf);
        return;
    }

    if (VV5430_WIDTH > ATLAS_X_IMG || ATLAS_X_IMG > MAX_X_IMG)
    {
        sprintf(_cBuf, "FATAL: image width dimension %d > %d > %d", VV5430_WIDTH, ATLAS_X_IMG, MAX_X_IMG);
        log(_cBuf);
        return;
    }
    if (VV5430_HEIGHT > ATLAS_Y_IMG || ATLAS_Y_IMG > MAX_Y_IMG)
    {
        sprintf(_cBuf, "FATAL: image height dimension %d > %d > %d", VV5430_HEIGHT, ATLAS_Y_IMG, MAX_Y_IMG);
        log(_cBuf);
        return;
    }

    if ((VV5430_WIDTH+_uBlackEdgeX) > ATLAS_X_IMG)
    {
        sprintf(_cBuf, "FATAL: black edge X (%d) too big, max is %d", _uBlackEdgeX, ATLAS_X_IMG-VV5430_WIDTH);
        log(_cBuf);
        return;
    }
    if ((VV5430_HEIGHT+_uBlackEdgeY) > ATLAS_Y_IMG)
    {
        sprintf(_cBuf, "FATAL: black edge Y (%d) too big, max is %d", _uBlackEdgeY, ATLAS_Y_IMG-VV5430_HEIGHT);
        log(_cBuf);
        return;
    }

    // initialize grabber
    _pVideoGrabber = new VideoGrabber(this, _bDebug, VV5430_WIDTH, VV5430_HEIGHT, _uSkipFrames);
    try
    {
        _pVideoGrabber->open(NULL);
    }
    catch(IIOException ex)
    {
        sprintf(_cBuf, "FATAL: %s", ex.what());
        log(_cBuf);
        return;
    }

    // initialize mux
    std::string sComPort = "/dev/" + getComPort();
    _pMuxUnit = new MuxUnit(sComPort, getBaudRate(), _bDebug, this);
    if (_pMuxUnit->getStatus() == false)
    {
        sprintf(_cBuf, "FATAL: Initialize TopMux failed: %s", _pMuxUnit->getLastError().c_str());
        log(_cBuf);
        return;
    }
    if (_pMuxUnit->resetMux() == false)
    {
        sprintf(_cBuf, "FATAL: resetMux failed: %s", _pMuxUnit->getLastError().c_str());
        log(_cBuf);
        return;
    }
    if (_pMuxUnit->getMuxVersion(_sMuxVersion) == false)
    {
        sprintf(_cBuf, "FATAL: getMuxVersion failed: %s", _pMuxUnit->getLastError().c_str());
        log(_cBuf);
        return;
    }
    sprintf(_cBuf, "Mux Version: %s", _sMuxVersion.c_str());

    (void)setTransWidth(_uDefTransWidth);

    std::thread	(_Abs, this).detach();

    log("Rasdim constructor finished successfully");

    _bStatus  = true;
}

/**
 *  \brief      Destructor of the Server class.
 *  \warning    Destructor is empty, it needs some implementation.
 *  \return     void
 */
Server::~Server()
{
    log("Rasdim destructor");
    for(int i = 0; i < MAX_REQUEST_LOOP; ++i)
    {
        if (bHandleRequest == false)
            break;
        ::sleep(1);
    }
    if (bHandleRequest == true)
        log("Rasdim destructor: bHandleRequest still true");
    delete _pRdMain;
    ::sleep(1);
}

/**
 *  \brief  Run the server by starting the necessary DIM servers and services.
 *
 *  If #_bStatus is true, i.e. the constructor was successful, the following DIM
 *  services and servers are started:
 *  \li DimServer: use #_sDnsNode as host for the DIM \e dns server
 *  \li DimService #_pImageService providing the exposed image, i.e. #_pImage.
 *  \li DimService #_pWindoutService providing the analysis results, i.e. #_pWindoutService
 *  \li DimRpc #_pRdMain: the \e key service of Rasdim. Instances of class #RdMain are derived
 *      from the class DimRpc and hence the #_pRdMain is started in a separate thread.
 *  \li DimServer: last DIM action, start the Rasdim server with a unique name, hence the use of the
 *      logical server number #_dServer.
 *
 *  Actually the main thread is finished now and paused. All further actions are taken over by the
 *  #_pRdMain thread.
 *
 *  \return void
 */
void Server::runServer()
{
    if (_bStatus != true)
        return;

    DimServer::setDnsNode(_sDnsNode.c_str());

    // Image service
    sprintf(_cBuf, "%s%d", RD_SRV_IMAGE, _dServer);
    _pImage = new Image;
    _pImageService = new DimService(_cBuf, RD_SRV_IMAGE_FORMAT, _pImage, sizeof(class Image));

    // Analysis stdout service
    sprintf(_cBuf, "%s%d", RD_SRV_WINDOUT, _dServer);
    _pWindoutService = new DimService(_cBuf, _pWindout);

    // Kernel RPC service
    sprintf(_cBuf, "%s%d", RD_RPC, _dServer);
    _pRdMain = new RdMain(this, _pVideoGrabber, _pMuxUnit, _cBuf, RD_RPC_FORMAT_IN, RD_RPC_FORMAT_OUT);

    // Start the Rasdim server
    sprintf(_cBuf, "%s%d", RD_SERVER, _dServer);
    DimServer::start(_cBuf);

    log("Rasdim server is up and running");

    while (bExitNow == false)
    {
        if (pause() == -1)
        {
            if (errno == EINTR)
            {
                log("Rasdim server interrupted by signal");
                break;
            }
        }
    }

    for(int i = 0; i < MAX_REQUEST_LOOP; ++i)
    {
        if (bHandleRequest == false)
            break;
        ::sleep(1);
    }
    if (bHandleRequest == true)
        log("Rasdim runServer: bHandleRequest still true");
    log("End of runServer");
}

/**
 *  \brief  Change the max-transition-width parameter of the grabber IP block.
 *
 *  The range of this parameter is between #MIN_TRANS_WIDTH and #MAX_TRANS_WIDTH (default = #_uDefTransWidth).
 *  The \e devmem utility is used (by means of \e system call) for this purpose.
 *  The grabber IP address of this parameter is #TRANS_WIDTH_ADDR.
 *  On success true is returned and #_uCurTransWidth is set, false otherwise.
 *
 *  \param  uTransWidth value of the max-transition-width
 *  \return bool
 */
bool Server::setTransWidth(const unsigned int uTransWidth)
{
    // Introduced in version 818
    unsigned int    uTwidth = uTransWidth;
    if (uTwidth < MIN_TRANS_WIDTH || uTwidth > MAX_TRANS_WIDTH)
    {
        sprintf(_cBuf, "WARNING: max-transmission-width %d illegal", uTransWidth);
        log(_cBuf);
        uTwidth = DFLT_TRANS_WIDTH;
    }
    sprintf(_cBuf, "devmem %s 32 0x%d", TRANS_WIDTH_ADDR, uTwidth);
    std::string sCommand = _cBuf;
    if (system(sCommand.c_str()) < 0)
    {
        sprintf(_cBuf, "ERROR: failed to execute: %s", sCommand.c_str());
        log(_cBuf);
        return false;
    }
    sprintf(_cBuf, "INFO: max-transition-width set to %d", uTwidth);
    log(_cBuf);
    _uCurTransWidth = uTwidth;
    return true;
}

/**
 *  \brief  The only log function of the Rasdim server.
 *
 *  The message, including a time-stamp, is appended to a log file.
 *  The location of the log file is determined by the parameter #_sLogDir and
 *  the name of the log file contains implicitly the date, hence for each day there is a separate log file.
 *
 *  \param  sMess   the message to be logged
 *  \return void
 */
void Server::log(const std::string& sMess)
{
    time_t  tNow;
    time(&tNow);
    struct  tm*     tminfo = localtime(&tNow);

    sprintf(_cBuf, "%02d%02d%02d", tminfo->tm_hour, tminfo->tm_min, tminfo->tm_sec);
    std::string sTime = _cBuf;
    sprintf(_cBuf, "%04d%02d%02d", tminfo->tm_year+1900, tminfo->tm_mon+1, tminfo->tm_mday);
    std::string sDate = _cBuf;

    std::string sLogFile = _sLogDir + "/rdlog_" + sDate + ".txt";
    if (prLog(sLogFile, sTime, sMess) == true)
        return;

    std::string sSecFile = SEC_LOG_FILE;
    sprintf(_cBuf, "ERROR: cannot write log file %s", sLogFile.c_str());
    if (prLog(sSecFile, sTime, _cBuf) == true)
    {
        (void)prLog(sSecFile, sTime, sMess);
    }
}

/**
 *  \brief  Write message with time into the appropriate log file.
 *
 *  Assistance method used by the #log method of this class.
 *  Return false if log file could not be opened. True otherwise.
 *
 *  \param  sLogFile    name of the log file
 *  \param  sTime       string containing the time, format HH:MM:SS
 *  \param  sMess       the message to be logged
 *
 *  \return bool
 */
bool Server::prLog(const std::string& sLogFile, const std::string& sTime, const std::string& sMess)
{
    FILE    *fp;
    fp = fopen(sLogFile.c_str(), "a");
    if (fp == NULL)
        return false;
    fprintf(fp, "%s: %s\n", sTime.c_str(), sMess.c_str());
    fclose(fp);
    return true;
}

/**
 *  \brief  Special log function to store channel which caused a permanent grabber timeout.
 *
 *  The special log file resides in the log area as defined by #_sLogDir.
 *
 *  \param  sChannel   the channel causing a permanent timeout
 *  \return void
 */
void Server::logGrabErrChannel(const std::string& sChannel)
{
	 time_t  tNow;
	 time(&tNow);
	 struct  tm*     tminfo = localtime(&tNow);

	 sprintf(_cBuf, "%02d%02d%02d", tminfo->tm_hour, tminfo->tm_min, tminfo->tm_sec);
	 std::string sTime = _cBuf;
	 sprintf(_cBuf, "%04d%02d%02d", tminfo->tm_year+1900, tminfo->tm_mon+1, tminfo->tm_mday);
	 std::string sDate = _cBuf;

	 std::string sGrabErrFile = _sLogDir + "/grabError.txt";
	 FILE    *fp;
	 fp = fopen(sGrabErrFile.c_str(), "a");
	 if (fp == NULL)
	     return;
	 fprintf(fp, "%s %s: %s\n", sDate.c_str(), sTime.c_str(), sChannel.c_str());
	 fclose(fp);
}

/**
 *  \brief  Get all configuration parameters from a file.
 *
 *  If the file does not exist or cannot be opened all parameters get there default value.
 *  It also gets the default value when the parameter is not present in the configuration file.
 *  The environment variable DIM_DNS_NODE overrules the default value of the #_sDnsNode parameter.
 *  The function makes use of an instance of the ConfigFile class to get the parameter values.
 *  It also makes sure that the #_uDefTransWidth parameter does not exceed its limits.
 *
 *  \param  sIniFile    name of the configuration (or ini) file
 *  \return void
 */
void Server::getConfigParams(const std::string& sIniFile)
{
    int         iVal;
    std::string sDfltDnsNode;
    char*   dim_dns_node = getenv("DIM_DNS_NODE");
    if (dim_dns_node == NULL || strlen(dim_dns_node) == 0)
        sDfltDnsNode = DFLT_DNS_HOST;
    else
        sDfltDnsNode = dim_dns_node;

    ConfigFile* pConfig = new ConfigFile();

    if (pConfig->chkIniFile(sIniFile) == false)
    {
        sprintf(_cBuf, "WARNING: cannot read init-file %s", sIniFile.c_str());
        log(_cBuf);
    }

    _dServer        = pConfig->getIntVal   (sIniFile, SERVER,         DFLT_SERVER);
    _sDnsNode       = pConfig->getStringVal(sIniFile, DNS_HOST,       sDfltDnsNode);
    _uGrabDelay     = pConfig->getIntVal   (sIniFile, GRAB_DELAY,     DFLT_GRAB_DELAY);
    _uI2cGrabDelay  = pConfig->getIntVal   (sIniFile, I2C_GRAB_DELAY, DFLT_I2C_GRAB_DELAY);
    _uSkipFrames    = pConfig->getIntVal   (sIniFile, SKIP_FRAMES,    DFLT_SKIP_FRAMES);
    _sComPort       = pConfig->getStringVal(sIniFile, COMPORT,        DFLT_COMPORT);
    _dBaudRate      = pConfig->getIntVal   (sIniFile, BAUDRATE,       DFLT_BAUDRATE);
    _sSoapDir       = pConfig->getStringVal(sIniFile, SOAP_DIR,       DFLT_SOAP_DIR);
    _sSoapExec      = pConfig->getStringVal(sIniFile, SOAP_EXEC,      DFLT_SOAP_EXEC);
    _sSoapHost      = pConfig->getStringVal(sIniFile, SOAP_HOST,      DFLT_SOAP_HOST);
    _dSoapPort      = pConfig->getIntVal   (sIniFile, SOAP_PORT,      DFLT_SOAP_PORT);
    _sSpotHost      = pConfig->getStringVal(sIniFile, SPOT_HOST,      DFLT_SPOT_HOST);
    _dSpotPort      = pConfig->getIntVal   (sIniFile, SPOT_PORT,      DFLT_SPOT_PORT);
    _sLogDir        = pConfig->getStringVal(sIniFile, LOG_DIR,        DFLT_LOG_DIR);
    _sImageDir      = pConfig->getStringVal(sIniFile, IMAGE_DIR,      DFLT_IMAGE_DIR);
    _sResultDir     = pConfig->getStringVal(sIniFile, RESULT_DIR,     DFLT_RESULT_DIR);
    _uBlackEdgeX    = pConfig->getIntVal   (sIniFile, BLACK_EDGE_X,   DFLT_BLACK_EDGE_X);
    _uBlackEdgeY    = pConfig->getIntVal   (sIniFile, BLACK_EDGE_Y,   DFLT_BLACK_EDGE_Y);
    iVal            = pConfig->getIntVal   (sIniFile, RASNIK_ENHANCE, DFLT_RASNIK_ENHANCE);
    _bRasnikEnhance = (iVal == 0 ? false : true);
    iVal            = pConfig->getIntVal   (sIniFile, SPOT_ENHANCE,   DFLT_SPOT_ENHANCE);
    _bSpotEnhance   = (iVal == 0 ? false : true);
    iVal            = pConfig->getIntVal   (sIniFile, DEBUG,          DFLT_DEBUG);
    _bDebug         = (iVal == 0 ? false : true);
    _uDefTransWidth = pConfig->getIntVal   (sIniFile, TRANS_WIDTH,    DFLT_TRANS_WIDTH);
    if (_uDefTransWidth < MIN_TRANS_WIDTH || _uDefTransWidth > MAX_TRANS_WIDTH)
    {
        _uDefTransWidth = DFLT_TRANS_WIDTH;
    }

    delete pConfig;
}

/**
 *  \brief  Write all configuration parameters, which are currently used, into a dump file.
 *  \param  sDumpFile    name of the dump file
 *  \return void
 */
void Server::dumpConfigParams(const std::string& sDumpFile)
{
    FILE    *fp;
    fp = fopen(sDumpFile.c_str(), "w");
    if (fp == NULL)
    {
        sprintf(_cBuf, "WARNING: cannot write dump-file %s", sDumpFile.c_str());
        log(_cBuf);
    }

    sprintf(_cBuf, "%-14s = %s", VERSION,        _sRdVersion.c_str());                  printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %d", SERVER,         _dServer);                             printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %s", DNS_HOST,       _sDnsNode.c_str());                    printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %d", GRAB_DELAY,     (int)_uGrabDelay);                     printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %d", I2C_GRAB_DELAY, (int)_uI2cGrabDelay);                  printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %d", SKIP_FRAMES,    (int)_uSkipFrames);                    printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %s", COMPORT,       _sComPort.c_str());                     printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %d", BAUDRATE,       (int)_dBaudRate);                      printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %s", SOAP_DIR,       _sSoapDir.c_str());                    printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %s", SOAP_EXEC,      _sSoapExec.c_str());                   printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %s", SOAP_HOST,      _sSoapHost.c_str());                   printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %d", SOAP_PORT,      _dSoapPort);                           printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %s", SPOT_HOST,      _sSpotHost.c_str());                   printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %d", SPOT_PORT,      _dSpotPort);                           printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %s", LOG_DIR,        _sLogDir.c_str());                     printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %s", IMAGE_DIR,      _sImageDir.c_str());                   printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %s", RESULT_DIR,     _sResultDir.c_str());                  printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %d", BLACK_EDGE_X,   (int)_uBlackEdgeX);                    printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %d", BLACK_EDGE_Y,   (int)_uBlackEdgeY);                    printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %d", RASNIK_ENHANCE, (_bRasnikEnhance == false ? 0 : 1));   printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %d", SPOT_ENHANCE,   (_bSpotEnhance == false ? 0 : 1));     printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %d", TRANS_WIDTH,    (int)_uDefTransWidth);                 printConfigParam(_cBuf, fp);
    sprintf(_cBuf, "%-14s = %d", DEBUG,          (_bDebug == false ? 0 : 1));           printConfigParam(_cBuf, fp);

    if (fp != NULL)
        fclose(fp);
}

/**
 *  \brief  Assistant function for the #dumpConfigParams method. The parameter string (name and value)
 *   are written to a file and also to the log file.
 *
 *  \param  sParam  string containing parameter name and value
 *  \param  fp      file pointer to opened file
 *  \return void
 */
void Server::printConfigParam(const std::string& sParam, FILE *fp)
{
    log(sParam);
    if (fp != NULL)
        fprintf(fp, "%s\n", sParam.c_str());
}


