/**
 *  \file   tiff.cpp
 *  \brief  Implementation source of the DCIFDEntry and DCImageHeader class.
 *          Used to create the header of a tiff file.
 */

#include "tiff.h"

/**
 *  \brief  Fill a IFD field of an images header.
 *  \param  tag
 *  \param  fieldtype
 *  \param  length
 *  \param  value
 *  \return void
 */
void DCIFDEntry::set(const short tag, const short fieldtype, const int length, const int value)
{
    Tag       = tag;
    FieldType = fieldtype;
    Length    = length;
    Value     = value;
}

/**
 *  \brief  Create an empty instance of class DCImageHeader.
 *  \return void
 */
DCImageHeader::DCImageHeader()
{
    this->clearHeader();
}

/**
 *  \brief  Create an instance of class DCImageHeader based on an image with given width and height.
 *  \param  iWidth the number of pixels horizontally
 *  \param  iHeight the number of pixels vertically
 *  \return void
 */
DCImageHeader::DCImageHeader(const int iWidth, const int iHeight)
{
    this->clearHeader();
    this->fillHeader(iWidth, iHeight);
}

/**
 *  \brief  Reset the entire instance of the image header.
 *  \return void
 */
void DCImageHeader::clearHeader()
{
    int     i;

    ByteOrder      = 0;
    TiffVersion    = 0;
    FirstIFDOffset = 0;

    RasVersion     = 0;
    IDataOffset    = 0;
    IDataSize      = 0;
    IImageWidth    = 0;
    IImageHeight   = 0;
    Reserved1      = 0;
    MDataOffset    = 0;
    MDataSize      = 0;
    MImageWidth    = 0;
    MImageHeight   = 0;

    IFDEntryCount  = 0;

    for(i = 0; i < IFD_ENTRIES; ++i)
        IFD[i].set(0, 0, 0, 0);

    NextIFDOffset  = 0;

    for(i = 0; i < N_RESOLUTIONS; ++i)
        Resolution[i] = 0;
    for(i = 0; i < N_RESERVED2; ++i)
        Reserved2[i] = 0;
}

/**
 *  \brief  Based on the given width and height, fill the header with the appropriate values.
 *          Look at https://en.wikipedia.org/wiki/TIFF for more details.
 *  \param  iWidth the number of pixels horizontally
 *  \param  iHeight the number of pixels vertically
 *  \return void
 */
void DCImageHeader::fillHeader(const int iWidth, const int iHeight)
{
    ByteOrder      = 0x4949;
    TiffVersion    = 0x002A;
    FirstIFDOffset = 0x0022;

    RasVersion     = 0x00534152;
    IDataOffset    = TIFF_HEADER_SIZE + iWidth * iHeight;
    IImageWidth    = iWidth;
    IImageHeight   = iHeight;

    IFDEntryCount  = IFD_ENTRIES;

    IFD[0].set(  NEW_SUBFILE_TYPE,           TIFF_LONG,     1, 0x0000);
    IFD[1].set(  IMAGE_WIDTH,                TIFF_LONG,     1, iWidth);
    IFD[2].set(  IMAGE_LENGTH,               TIFF_LONG,     1, iHeight);
    IFD[3].set(  BITS_PER_SAMPLE,            TIFF_SHORT,    1, 0x0008);
    IFD[4].set(  COMPRESSION,                TIFF_SHORT,    1, 0x0001);
    IFD[5].set(  PHOTOMETRIC_INTERPRETATION, TIFF_SHORT,    1, 0x0001);
    IFD[6].set(  STRIP_OFFSETS,              TIFF_LONG,     1, 0x0100);
    IFD[7].set(  SAMPLES_PER_PIXEL,          TIFF_SHORT,    1, 0x0001);
    IFD[8].set(  ROWS_PER_STRIP,             TIFF_LONG,     1, iHeight);
    IFD[9].set(  STRIP_BYTE_COUNTS,          TIFF_LONG,     1, iWidth * iHeight);
    IFD[10].set( X_RESOLUTION,               TIFF_RATIONAL, 1, 0x00C4);
    IFD[11].set( Y_RESOLUTION,               TIFF_RATIONAL, 1, 0x00CC);
    IFD[12].set( RESOLUTION_UNIT,            TIFF_SHORT,    1, 0x0001);

    NextIFDOffset = 0x0;

    for(int i = 0; i < N_RESOLUTIONS; ++i)
        Resolution[i] = 1;
}

