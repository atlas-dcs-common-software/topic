/**
 *  \file   videograbber.cpp
 *  \brief  Implementation of the VideoGrabber class.
 *          Original source by Mike.Looijmans\@TOPIC.
 *  \author Robert.Hart@nikhef.nl
 *  \date   June 2016
 */

#include <iostream>
#include <string.h>   // necessary for memset
#include <stdio.h>
#include <unistd.h>
#include <iio.h>
#include <errno.h>

#include "videograbber.h"
#include "server.h"

/**
 *  \brief  The (only) constructor of the VideoGrabber class.
 *
 *  The protected members of this class are set to their default values or taken
 *  over from the arguments. A buffer for the image is allocated and assigned to #_pData.
 *  This buffer is initially filled with an artificial image, showing a picture of all black
 *  an white values from top to bottom.
 *
 *  \param  pServer pointer to an object (the caller) of the Server class
 *  \param  bDebug on if true, off otherwise
 *  \param  xWidth the number of pixels horizontally
 *  \param  yHeight number of pixels vertically
 *  \param  skipFrames number of frames to be skipped before grabbing
 *  \return void
 */
VideoGrabber::VideoGrabber(Server* pServer, const bool bDebug, const unsigned int xWidth, const unsigned int yHeight, const unsigned int skipFrames)
{
    _pServer       = pServer;
    _bDebug        = bDebug;
    _bStatus       = false;
    _bIsOpen       = false;
    _bReOpen       = false;
    _xWidth        = xWidth;
    _yHeight       = yHeight;
    _iImgSize      = xWidth * yHeight;
    _pData         = new unsigned char[_iImgSize];
    _pIio_ctx      = NULL;
    _pIio_dev      = NULL;
    _pIio_buf      = NULL;
    _nImgCounter   = 0;
    _nTimeout      = 0;
    _nSkip         = skipFrames;
    if (_nSkip > MAX_SKIP_INIT_FRAMES)
        _nSkip = MAX_SKIP_INIT_FRAMES;

    unsigned int   i;
    unsigned int   stride = _iImgSize >> 8;

    for (i = 0; i < 256; ++i)
    {
        memset(_pData + (i*stride), i, stride);
    }
    memset(_pData + (255*stride), 255, (_iImgSize - (255*stride)));

    _bStatus = true;
}

/**
 * \brief   Destructor of the VideoGrabber class.
 *          Calls the close() method and frees the image buffer #_pData.
 * \return  void
 */
VideoGrabber::~VideoGrabber()
{
    close();
    delete [] _pData;
}

/**
 * \brief   Creates an iio_context and opens an iio_device.
 *
 * As a kind of redundancy measure, it calls the close() method first.
 * If host == NULL it will create a local iio_context, otherwise
 * a remote context. On error an IIOException is thrown.
 *
 * \param   host name or IP of the remote host, if NULL it will be local
 * \return  void
 */
void VideoGrabber::open(const char *host)
{
    close();

    const   char*   sDevicename = IIO_DEVICE_NAME;

    if (host)
    {
        _bUseHost = true;
        _sHost    = host;
        _pIio_ctx = iio_create_network_context(host);
    }
    else
    {
        _bUseHost = false;
        _pIio_ctx = iio_create_default_context();
    }
    if (!_pIio_ctx)
        throw IIOException("VideoGrabber: failed to open context", (host ? host : "(local)"));

    _pIio_dev = iio_context_find_device(_pIio_ctx, sDevicename);
    if (!_pIio_dev)
        throw IIOException("VideoGrabber: failed to find device", sDevicename);

    int n_chn = iio_device_get_channels_count(_pIio_dev);
    if (n_chn < 1)
        throw IIOException("VideoGrabber: no channels found");

    for (int i = 0; i < n_chn; ++i)
    {
        struct iio_channel *iio_chn = iio_device_get_channel(_pIio_dev, i);
        if (iio_channel_is_scan_element(iio_chn))
            iio_channel_enable(iio_chn);
    }

    _bIsOpen = true;
}

/**
 * \brief   Closes all iio-pointers (buffer, device and context) and sets them to NULL.
 * \return  void
 */
void VideoGrabber::close()
{
    _bIsOpen = false;
    _bReOpen = false;

    if (_pIio_buf)
    {
        iio_buffer_destroy(_pIio_buf);
        _pIio_buf = NULL;
    }
    if (_pIio_dev) {
        /*iio_device_destroy(_pIio_dev);*/
        _pIio_dev = NULL;
    }
    if (_pIio_ctx)
    {
        iio_context_destroy(_pIio_ctx);
        _pIio_ctx = NULL;
    }
}

/**
 * \brief   Grab an image. Read the ADC by allocating an iio_buffer.
 *
 * Key method of the VideoGrabber class. If #_bReOpen is true (caused by a previous
 * grab which failed due to a timeout), it calls the open() method first. It starts the ADC
 * by creating an iio_buffer (#_pIio_buf). NB: this buffer has to be closed (it stops the ADC)
 * before leaving this method.
 * Before an image is grabbed, a number (i.e. #_nSkip) of frames are taken and skipped,
 * because the first frames could be from the previous channel and/or are not synchronized yet.
 * If however a frame could not be fetched (no signal available), then after 2 seconds
 * a timeout is raised (implemented by a select call) and the #_bReOpen boolean is set to true.
 * Furthermore it is vital (as mentioned before) that when a timeout is detected, the
 * #_pIio_buf pointer has to be closed, otherwise the ADC may enter into a loop which can only
 * be resolved by restarting the board.
 * On success, the last frame is grabbed, but before it is stored into #_pData, the data has
 * to be transformed from the 16-bit ADC into an equivalent 8-bit black/white image. There
 * are 2 ways of transformation, depending on the bEnhance parameter. If false (the default)
 * a linear transformation is used. If true, the entire image is enhanced by looking for
 * the brightest and darkest pixel and these values are used as maximum 0x0 and minimum 0xff
 * of the 8-bit image.
 *
 * \param   grabDelay   Delay time in milliseconds before the first image is grabbed
 * \param   bEnhance    If true the image is enhanced
 *
 * \return  void (On error an IIOException is thrown)
 */
void VideoGrabber::grab(const unsigned long grabDelay, const bool bEnhance)
{
    unsigned long   ulGrabDelay = grabDelay;
    if (ulGrabDelay > MAX_GRAB_DELAY)
        ulGrabDelay = MAX_GRAB_DELAY;
    else if (ulGrabDelay < MIN_GRAB_DELAY)
        ulGrabDelay = MIN_GRAB_DELAY;
    usleep(ulGrabDelay * 1000); // grabDelay is given in milliseconds

    if (_bReOpen == true)
    {
        // caused by a previous timeout
        if (_bUseHost == true)
            open(_sHost.c_str());
        else
            open(NULL);
        if (_bIsOpen == true)
            _pServer->log("VideoGrabber reopened due to timeout");
    }

    if (_bIsOpen == false)
        throw IIOException("VideoGrabber: not opened yet, call open() first");

    if (_pIio_buf)
    {
        // redundancy check
        iio_buffer_destroy(_pIio_buf);
        _pIio_buf = NULL;
    }

    _pIio_buf = iio_device_create_buffer(_pIio_dev, _iImgSize, false);
    if (!_pIio_buf)
        throw IIOException("VideoGrabber: iio_device_create_buffer() failed");

    if (_nSkip == 0)
        _nSkip = 1;

    /* The buffer is non-blocking, so we use select() to
     * check if it has data available.
     */

    fd_set  rfds;
    struct  timeval tv;
    int     fd = iio_buffer_get_poll_fd(_pIio_buf);

    FD_ZERO(&rfds);
    FD_SET(fd, &rfds);
    // wait up to 2 seconds
    tv.tv_sec  = 2;
    tv.tv_usec = 0;

    for(unsigned int i = 0; i < _nSkip; ++i)
    {
        int r = select(fd+1, &rfds, NULL, NULL, &tv);

        // Negative result means something went wrong
        if (r < 0)
        {
            if (_pIio_buf)
            {
                iio_buffer_destroy(_pIio_buf);
                _pIio_buf = NULL;
            }
            throw IIOException("VideoGrabber: select() failed on IIO buffer");
        }

        // Zero result means the timeout elapsed (no data)
        if (r == 0)
        {
            ++_nTimeout;
            _bReOpen = true;
            // v808, close buffer before throwing exception
            if (_pIio_buf)
            {
                iio_buffer_destroy(_pIio_buf);
                _pIio_buf = NULL;
            }
            throw IIOException("VideoGrabber: timeout reading IIO buffer");
        }

        // There is data on the only fd in the set
        int nBytes_rx = iio_buffer_refill(_pIio_buf);
        if (nBytes_rx < 0)
        {
            // If we get an EGAIN error, something interrupted the select() call and there
            // was no data. In that case, try again.
            if (nBytes_rx == -EAGAIN)
                continue;
            // otherwise, something went really wrong
            if (_pIio_buf)
            {
                iio_buffer_destroy(_pIio_buf);
                _pIio_buf = NULL;
            }
            throw IIOException("VideoGrabber: failed to read from IIO buffer");
        }
    }

    // Success: image grabbed!
    const unsigned  short   *raw = (const unsigned short*)iio_buffer_start(_pIio_buf);

    unsigned short  s_min;
    unsigned short  s_max;

    if (bEnhance == false)
    {
        // linear conversion
        s_min = 0;
        s_max = 0xFFFF;
    }
    else
    {
        // linear conversion, based on weakest and brightest pixel
        s_min = *raw;
        s_max = *raw;

        for (unsigned int i = 1; i < _iImgSize; ++i)
        {
            const unsigned  short   s = raw[i];
            if (s < s_min)
                s_min = s;
            else if (s > s_max)
                s_max = s;
        }
    }

    if (s_max == s_min)
    {
        /* Display single color when all pixels are equal */
        memset(_pData, s_min >> 8, _iImgSize);
    }
    else
    {
        const unsigned  int sample_mult = 0xFFFFFFFF / (s_max - s_min);
        for (unsigned int i = 0; i < _iImgSize; ++i)
        {
            _pData[i] = ((raw[i] - s_min) * sample_mult) >> 24;
        }
    }

    if (_pIio_buf)
    {
        iio_buffer_destroy(_pIio_buf);
        _pIio_buf = NULL;
    }

    ++_nImgCounter;
}
