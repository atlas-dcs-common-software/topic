/**
 *  \file   interface.h
 *  \brief  Include file containing the interface between the \e Rasdim server and its clients.
 *
 *  \author Robert.Hart@nikhef.nl
 *  \date   First version: 2004
 */

#ifndef INTERFACE_H
#define INTERFACE_H

// Make sure NO_ERROR is defined and equals 0.
#ifndef NO_ERROR
#define NO_ERROR                0
#endif
#if     NO_ERROR!=0
#error  "NO_ERROR!=0"
#endif

// Rasdim Errors
#define RDE_ILLCMD              1           /**< Rasdim error: illegal command  */
#define RDE_MUX                 2           /**< Rasdim error: TopMux */
#define RDE_GRABBER             3           /**< Rasdim error: grabber */
#define RDE_ANALYSIS            4           /**< Rasdim error: analysis */
#define RDE_OUTPUT              5           /**< Rasdim error: output file */
#define RDE_WRONGSERVER         6           /**< Rasdim error: wrong server addressed */
#define RDE_ILLBKGRND           7           /**< Rasdim error: background subtraction */
#define RDE_NOANALYSIS          11          /**< Rasdim error: analysis server unreachable */
// next error codes are used by clients only
#define RDE_NOSERVER            8           /**< Rasdim error: server not running */
#define RDE_TIMEOUT             9           /**< Rasdim error: timeout on server */
#define RDE_INTERNAL            10          /**< Rasdim error: internal client error */

// Commands
#define CMD_QUIT                0x0100      /**< Rasdim command: quit server */
#define CMD_PROBE               0x0101      /**< Rasdim command: probe server */
#define CMD_LED_ON              0x0102      /**< Rasdim command: led on (obsolete) */
#define CMD_LED_OFF             0x0103      /**< Rasdim command: led off (obsolete) */
#define CMD_VIEW                0x0104      /**< Rasdim command: view channel (obsolete) */
#define CMD_GRAB                0x0105      /**< Rasdim command: grab image */
#define CMD_ANAL                0x0106      /**< Rasdim command: grab and analyze image */
#define CMD_MUX_OFF             0x0107      /**< Rasdim command: reset TopMux */

// Auxiliary commands (masks)
#define AUX_SAVE_IMAGE          0x010       /**< Rasdim auxiliary command: save image */
#define AUX_DONT_SAVE_BADIMAGE  0x020       /**< Rasdim auxiliary command: do not save image if analysis fails (#RDE_ANALYSIS) */

// DIM definitions
#define RD_SERVER               "Rasdim"                        /**< DIM name of Rasdim server */
#define RD_RPC                  "RD_Command"                    /**< DIM name of RPC service */
#define RD_RPC_FORMAT_IN        "I:2;C:32;I:79;F:32"            /**< Format belonging to class #Command */
#define RD_RPC_FORMAT_OUT       "C:32;I:5;C:20;F:8;I:1;F:16"    /**< Format belonging to class #Result */
#define RD_SRV_IMAGE            "RD_Image"                      /**< DIM name of Image service */
#define RD_SRV_IMAGE_FORMAT     "C:128;I:2;C"                   /**< Format belonging to class #Image */
#define RD_SRV_WINDOUT          "RD_Windout"                    /**< DIM-name of Windout (stdout) service */
#define RD_SRV_WINDOUT_FORMAT   "C"                             /**< Format of Windout service (\c char) */

#define MAX_CHAN_NAME           31                              /**< Maximum length of channel name */
#define MAX_INFO                127                             /**< Maximum length of info line of class #Image */
#define MAX_DATE_STR            19                              /**< Maximum length of date and time string */
#define MAX_SPOTS               8                               /**< Maximum number of spots (only if Spot analysis is applied) */
#define MAX_WINDOUT             2048                            /**< Maximum length of Windout (stdout) line */

#define MAX_NIN                 32      /**< Maximum number of \e integer parameters for analysis */
#define MAX_FIN                 32      /**< Maximum number of \e float parameters for analysis */

// maximum dimension of images */
#define MAX_X_IMG               768     /**< Maximum horizontal image pixel size */
#define MAX_Y_IMG               576     /**< Maximum vertical image pixel size */

// alignment dimensions for the analysis
#define ATLAS_X_IMG             392     /**< Horizontal image pixel size as used by the barrel alignment of ATLAS */
#define ATLAS_Y_IMG             292     /**< Vertical image pixel size as used by the barrel alignment of ATLAS */

// dimensions of the Rasnik camera's
#define VV5430_WIDTH            384     /**< Horizontal pixel size of the VV5430 CMOS sensor */
#define VV5430_HEIGHT           287     /**< Vertical pixel size of the VV5430 CMOS sensor */

// Analysis types
#define T_RASNIK                0       /**< Analysis type Rasnik (the epoch version) */
#define T_SPOT                  1       /**< Analysis type Spot */
#define T_FOAM                  2       /**< Analysis type FOAM (FOrier Analysis Method) */
#define T_SEFO                  4       /**< Analysis type SEFO (SEcond FOam) */
#define T_SOAP                  8       /**< Analysis type SOAP (aka SEFO) */
#define T_DOAP                  16      /**< Analysis type DOAP (used for diffraction images) */

// Predefined indices for Nin
#define I_NIN_COUNT             0       /**< Index inside _Nin array: Number of valid integer parameters */
#define I_FIN_COUNT             1       /**< Index inside _Nin array: Number of valid float parameters */
#define I_VERSION               2       /**< Index inside _Nin array: Version number of analysis (not used anymore) */
#define I_X_IMG                 3       /**< Index inside _Nin array: Horizontal image pixel size */
#define I_Y_IMG                 4       /**< Index inside _Nin array: Vertical image pixel size */

/**
 *  \typedef    BYTE    unsigned char
 *  \brief      The images to be analyzed consist of BYTE's.
 */
typedef unsigned char   BYTE;           /**< The images to be analyzed consist of BYTEs */

/**
 *  \class  MuxAddress
 *  \brief  This class contains the full address of the channel (i.e. camera and led).
 */
class MuxAddress
{
public:
    int         _LedServer;
    int         _LedTopMux;
    int         _LedMasterMux;
    int         _LedRasMux;
    int         _CamServer;
    int         _CamTopMux;
    int         _CamMasterMux;
    int         _CamRasMux;
};

/**
 *  \class  I2c
 *  \brief  This class contains all I2C parameters of a channel. An explanation of the various parameters is not given.
 *          For details look at the VV5430 CMOS image sensor documentation.
 */
class I2c
{
public:
    int         _rSetup1;
    int         _Normalbacklit;
    int         _Linearmode;
    int         _Autogain;
    int         _Inhibitblack;
    int         _Enableauto;
    int         _Horshuffle;
    int         _Vershuffle;
    int         _Forceblack;
    int         _Div0;
    int         _Div1;

    int         _rSetup2;
    int         _Readmode;
    int         _Pixelmode;
    int         _Pixthreshold;
    int         _Shufflemode;

    int         _rSetup3;
    int         _Pixeloffset;
    int         _Enablesno;
                
    int         _rAnalog;
    int         _Antiblooming;
    int         _Blackreference;
    int         _Whitethreshold;
    int         _Binairisation;

    int         _rGain,   _Gain;
    int         _rCoarse, _Coarse;
    int         _rFine,   _Fine;
    int         _rLower,  _Lower;
    int         _rUpper,  _Upper;
};

/**
 *  \class  Param
 *  \brief  This class contains the analysis arguments of a channel.
 */
class Param
{
public:
    int         _AnalType;
    int         _Nin[MAX_NIN];
    float       _Fin[MAX_FIN];
};

/**
 *  \class  Command
 *  \brief  This class, with only public variables and no methods, represents a \e request as send by
 *          a client and received by a Rasdim server.
 */
class Command
{
public:
    int         _Cmd;
    int         _AuxCmd;
    char        _Channel[MAX_CHAN_NAME+1];
    int         _ChannelId;
    int         _SequenceNr;
    MuxAddress  _MuxAddress;
    int         _Background;
    int         _NImages;
    I2c         _I2c;
    Param       _Param;
};

/**
 *  \class  Result
 *  \brief  This class, with only public variables and no methods, represents a \e reply as returned by
 *          a Rasdim server to a client.
 */
class Result
{
public:
    char        _Channel[MAX_CHAN_NAME+1];
    int         _ChannelId;
    int         _SequenceNr;
    int         _Result;
    int         _AnalResult;
    long        _Time;
    char        _CTime[MAX_DATE_STR+1];
    float       _X;
    float       _Y;
    float       _Scale;
    float       _RotZ;
    float       _ErrX;
    float       _ErrY;
    float       _ErrScale;
    float       _ErrRotZ;
    int         _NSpots;
    float       _XSpot[MAX_SPOTS];
    float       _YSpot[MAX_SPOTS];
};

/**
 *  \class  Image
 *  \brief  This class is used to expose/publish the grabbed image by means of a DimService.
 */
class Image
{
public:
    char        _Info[MAX_INFO+1];
    int         _Ximg;
    int         _Yimg;
    BYTE        _Image[MAX_X_IMG*MAX_Y_IMG];
};

#endif  /*INTERFACE_H*/


