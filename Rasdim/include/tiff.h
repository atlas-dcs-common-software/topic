/**
 *  \file   tiff.h
 *  \brief  Include file containing the DCIFDEntry and DCImageHeader class. Based on a original source from P.Verloop. Used to
 *          create tiff images.
 *  \author Robert.Hart@nikhef.nl
 */

#include <stdio.h>
#include <string.h>

#define TIFF_HEADER_SIZE              0x100    // 256

#define TIFF_SHORT                    3
#define TIFF_LONG                     4
#define TIFF_RATIONAL                 5

#define IFD_ENTRIES                   13

#define NEW_SUBFILE_TYPE              0x00FE
#define IMAGE_WIDTH                   0x0100
#define IMAGE_LENGTH                  0x0101
#define BITS_PER_SAMPLE               0x0102
#define COMPRESSION                   0x0103
#define PHOTOMETRIC_INTERPRETATION    0x0106
#define STRIP_OFFSETS                 0x0111
#define SAMPLES_PER_PIXEL             0x0115
#define ROWS_PER_STRIP                0x0116
#define STRIP_BYTE_COUNTS             0x0117
#define X_RESOLUTION                  0x011A
#define Y_RESOLUTION                  0x011B
#define RESOLUTION_UNIT               0x0128

#define N_RESOLUTIONS                 4
#define N_RESERVED2                   44

/**
 *  \class  DCIFDEntry
 *  \brief  Assistant class for DCImageHeader class.
 */
class DCIFDEntry
{
public:
    void    set(const short tag, const short fieldtype, const int length, const int value);

private:
    short   Tag;
    short   FieldType;
    int     Length;
    int     Value;
};

/**
 *  \class  DCImageHeader
 *  \brief  Header of a tiff file.
 */
class DCImageHeader
{
public:
    DCImageHeader();
    DCImageHeader(const int iWidth, const int iHeight);

    short       ByteOrder;                 // 0x0000
    short       TiffVersion;               // 0x0002
    int         FirstIFDOffset;            // 0x0004

    int         RasVersion;                // 0x0008
    int         IDataOffset;               // 0x000C
    short       IDataSize;                 // 0x0010
    short       IImageWidth;               // 0x0012
    short       IImageHeight;              // 0x0014
    short       Reserved1;                 // 0x0016
    int         MDataOffset;               // 0x0018
    short       MDataSize;                 // 0x001C
    short       MImageWidth;               // 0x001E
    short       MImageHeight;              // 0x0020

    short       IFDEntryCount;             // 0x0022
    DCIFDEntry  IFD[IFD_ENTRIES];          // 0x0024
    int         NextIFDOffset;             // 0x00C0

    int         Resolution[N_RESOLUTIONS]; // 0x00C4
    char        Reserved2[N_RESERVED2];    // 0x00D4

private:
    void        clearHeader();
    void        fillHeader(const int iWidth, const int iHeight);
};


