/**
 *  \file   comport.h
 *  \brief  Include file containing the ComPort class.
 *  \author Robert.Hart@nikhef.nl
 *  \date   May 2014
 */

#define MAX_BYTES   1024    /**< size of temporary buffer */

class Server;   // forward declaration

/**
 *  \class  ComPort
 *  \brief  This class implements the connection between the RS232 interface of the Topic Board
 *          and the TopMux. Previous versions of this class ran on Windows machines, hence the
 *          name ComPort.
 *
 *  Reading from and writing to the ComPort is handled by this class.
 *  The constructor opens it after which communication can take place.
 *  On error the #_sLastError string is filled, which can be obtained by the #getLastError method.
 *
 *  \author Robert.Hart@nikhef.nl
 *  \date   Feb 2016, implemented on TOPIC
 */
class ComPort
{
public:
            ComPort(const std::string& portName, const long baudRate, const bool debug, Server* pServer);
           ~ComPort();

    bool            readComPort(std::string& str);
    bool            writeComPort(const std::string& str);

    bool            getStatus()    { return _bStatus;    }  /**< Return #_bStatus */
    bool            getDebug()     { return _bDebug;     }  /**< Return #_bDebug */
    std::string     getLastError() { return _sLastError; }  /**< Return #_sLastError */
    std::string     getComPort()   { return _sComPort;   }  /**< Return #_sComPort */
    long            getBaudRate()  { return _dBaudRate;  }  /**< Return #_dBaudRate */

    void            dbgString(const std::string& hdr, const std::string& str);

private:
    int             _Fd;                /**< File descriptor to the ComPort (/dev/tty..) */
    bool            _bStatus;           /**< True if the constructor finished successfully */
    bool            _bDebug;            /**< Debug flag: on if true, off otherwise */
    Server*         _pServer;           /**< Pointer to the #Server class */
    std::string     _sLastError;        /**< String containing the last error (if any) */
    std::string     _sComPort;          /**< Name of the ComPort (or /dev/tty...) */
    long            _dBaudRate;         /**< The baudrate of the connection */
    char            _cBuf[MAX_BYTES];   /**< Temporary buffer */
};

