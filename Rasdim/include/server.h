/**
 *  \file   server.h
 *  \brief  Include file containing the Server class
 *  \author Robert.Hart@nikhef.nl
 *  \date   November 2015
 */

#ifndef SERVER_H_
#define SERVER_H_

#include <string>
#include "interface.h"

#define BUF_SIZE    1024    /**< size of error string */

// forward declarations
class DimService;
class VideoGrabber;
class MuxUnit;
class RdMain;

/**
 *  \class Server
 *  \brief Unfolds and starts the Rasdim server. It takes care of the initialization and
 *         creation of the necessary objects.
 *
 *  The Server class instantiates several necessary objects and defines and
 *  sets several configuration variables (mainly from a so-called config or .ini file).
 *  For its communication with the outside world it uses DIM. For more information on DIM look at:
 *  http://dim.web.cern.ch/dim/
 *
 *  \author Robert.Hart@nikhef.nl
 *  \date   November 2015
 */
class Server
{
public:
                    Server();
                   ~Server();

    void            runServer();
    void            log(const std::string& sMess);
    void            logGrabErrChannel(const std::string& sChannel);
    bool            setTransWidth(const unsigned int uTransWidth);

    bool            getStatus()       { return _bStatus;       }    /**< Returns #_bStatus */
    bool            getDebug()        { return _bDebug;        }    /**< Returns #_bDebug */
    int             getServer()       { return _dServer;       }    /**< Returns #_dServer */
    std::string     getSoapDir()      { return _sSoapDir;      }    /**< Returns #_sSoapDir */
    std::string     getSoapExec()     { return _sSoapExec;     }    /**< Returns #_sSoapExec */
    std::string     getSoapHost()     { return _sSoapHost;     }    /**< Returns #_sSoapHost */
    std::string     getLocalSoapHost(){ return _sLocalSoapHost;}    /**< Returns #_sLocalSoapHost */
    int             getSoapPort()     { return _dSoapPort;     }    /**< Returns #_dSoapPort */
    std::string     getSpotHost()     { return _sSpotHost;     }    /**< Returns #_sSpotHost */
    int             getSpotPort()     { return _dSpotPort;     }    /**< Returns #_dSpotPort */
    long            getBaudRate()     { return _dBaudRate;     }    /**< Returns #_dBaudRate */
    unsigned long   getGrabDelay()    { return _uGrabDelay;    }    /**< Returns #_uGrabDelay */
    unsigned long   getI2cGrabDelay() { return _uI2cGrabDelay; }    /**< Returns #_uI2cGrabDelay */
    unsigned int    getSkipFrames()   { return _uSkipFrames;   }    /**< Returns #_uSkipFrames */
    std::string     getComPort()      { return _sComPort;      }    /**< Returns #_sComPort */
    std::string     getVersion()      { return _sRdVersion;    }    /**< Returns #_sRdVersion */
    std::string     getDnsNode()      { return _sDnsNode;      }    /**< Returns #_sDnsNode */
    std::string     getLogDir()       { return _sLogDir;       }    /**< Returns #_sLogDir */
    std::string     getImageDir()     { return _sImageDir;     }    /**< Returns #_sImageDir */
    std::string     getResultDir()    { return _sResultDir;    }    /**< Returns #_sResultDir */
    std::string     getMuxVersion()   { return _sMuxVersion;   }    /**< Returns #_sMuxVersion */
    unsigned int    getBlackEdgeX()   { return _uBlackEdgeX;   }    /**< Returns #_uBlackEdgeX */
    unsigned int    getBlackEdgeY()   { return _uBlackEdgeY;   }    /**< Returns #_uBlackEdgeY */
    bool            getRasnikEnhance(){ return _bRasnikEnhance;}    /**< Returns #_bRasnikEnhance */
    bool            getSpotEnhance()  { return _bSpotEnhance;  }    /**< Returns #_bSpotEnhance */
    unsigned int    getCurTransWidth(){ return _uCurTransWidth;}    /**< Returns #_uCurTransWidth */
    unsigned int    getDefTransWidth(){ return _uDefTransWidth;}    /**< Returns #_uDefTransWidth */

    Image*          _pImage;                /**< Pointer to the grabbed image */
    char            _pWindout[MAX_WINDOUT]; /**< Text-buffer of the last analysis */
    DimService*     _pImageService;         /**< DimService to the exposed image, i.e. #_pImage */
    DimService*     _pWindoutService;       /**< DimService to the last analysis text-buffer, i.e. #_pWindout */

private:
    bool            _bStatus;           /**< True if constructor is finished */
    bool            _bDebug;            /**< Debug flag: on if true, off otherwise */
    RdMain*         _pRdMain;           /**< Pointer to an instance of the RdMain class */
    VideoGrabber*   _pVideoGrabber;     /**< Pointer to an instance of the #VideoGrabber class */
    MuxUnit*        _pMuxUnit;          /**< Pointer to an instance of the MuxUnit class */
    std::string     _sRdVersion;        /**< Rasdim version string */
    char            _cBuf[BUF_SIZE];    /**< Temporary buffer for error strings */

    int             _dServer;           /**< Logical (and unique!) number of the Rasdim server */
    std::string     _sSoapHost;         /**< Hostname or IP of machine Soap server is running on */
    std::string     _sLocalSoapHost;    /**< Hostname of machine Rasdim server is running on */
    int             _dSoapPort;         /**< Port number of the Soap-server */
    std::string     _sSpotHost;         /**< Hostname or IP of machine Spot-server is running on */
    int             _dSpotPort;         /**< Port number of the Spot-server */
    std::string     _sSoapDir;          /**< Directory name in which the Soap server resides */
    std::string     _sSoapExec;         /**< Name of the Soap server: a java .jar file */
    std::string     _sComPort;          /**< Name of the terminal connection to the TopMux */
    long            _dBaudRate;         /**< Baudrate of the serial connection to the TopMux */
    unsigned long   _uGrabDelay;        /**< Delay time (in msec) between switching the channel and grabbing an image */
    unsigned long   _uI2cGrabDelay;     /**< Same as #_uGrabDelay, but for channels applying I2C settings */
    unsigned int    _uSkipFrames;       /**< Number of frames to be skipped, before getting an image */
    std::string     _sDnsNode;          /**< Hostname or IP of machine dns is running on */
    std::string     _sLogDir;           /**< Name of directory containing the log files */
    std::string     _sImageDir;         /**< Name of directory containing the images as .tif files */
    std::string     _sResultDir;        /**< Name of directory containing the analysis results */
    std::string     _sMuxVersion;       /**< Firmware version of the TopMux */
    unsigned int    _uBlackEdgeX;       /**< Number of black pixels on left side of an image */
    unsigned int    _uBlackEdgeY;       /**< Number of black lines on top of an image */
    bool            _bRasnikEnhance;    /**< If true, apply enhancement on images of type Rasnik */
    bool            _bSpotEnhance;      /**< If true, apply enhancement on images of type Spot */
    unsigned int    _uCurTransWidth;    /**< Current value of the max-transition-width of frame grabber IP block */
    unsigned int    _uDefTransWidth;    /**< Default value of the max-transition-width of frame grabber IP block */

    bool            prLog(const std::string& sLogFile, const std::string& sTime, const std::string& sMess);
    void            getConfigParams (const std::string& sIniFile);
    void            dumpConfigParams(const std::string& sDumpFile);
    void            printConfigParam(const std::string& sParam, FILE *fp);
};

#endif /* SERVER_H_ */
