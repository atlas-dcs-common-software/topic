/**
 *  \file   rdmain.h
 *  \brief  Include file containing the RdMain class
 *  \author Robert.Hart@nikhef.nl
 *  \date   November 2015
 */

#ifndef RDMAIN_H
#define RDMAIN_H

#include <dic.hxx>
#include <dis.hxx>
#include <string>

#include "interface.h"

#define BUF_SIZE        1024    /**< size of error string */
#define DOUT_SIZE       9       /**< size of analysis output array */

// forward declarations
class Server;
class VideoGrabber;
class MuxUnit;
class Command;
class Result;

/**
 *  \class  RdMain
 *
 *  \brief  Heart of the Rasdim server. Incoming requests are decoded, handled accordingly and replied.
 *
 *  The RdMain class is the \e heart of the Rasdim server. Its main task is to carry out \e analyze-channel
 *  requests from any client. The following steps will take place when such a request is received:
 *  \li Using a #MuxUnit instance: switch the camera and led (and optional any I2C setting)
 *  \li Using a #VideoGrabber instance: Grab an image
 *  \li Depending on the type of channel: analyze the image by the Soap-server or Spot analysis library
 *  \li Send the results back to the client
 *
 *  There are 2 types of analysis:
 *  \li \b Soap: written in Java and implemented as server. Communication is done by \e libcurl.
 *               In case the server is running remotely and fails, the server is started locally
 *               and the acquisition should continue.
 *  \li \b Spot: written in C and C++ and available as server. Communication is done by \e libcurl.
 *
 *  \author Robert.Hart@nikhef.nl
 *  \date   May 2014
 */
class RdMain : public DimRpc
{
public:
    RdMain(Server* pServer, VideoGrabber* pVideoGrabber, MuxUnit* pMuxUnit,
           const char* fmName, const char* fmIn, const char* fmOut);
   ~RdMain();

private:
    Server*         _pServer;           /**< Pointer to the #Server calling this object */
    VideoGrabber*   _pVideoGrabber;     /**< Pointer to the instance of the #VideoGrabber class */
    MuxUnit*        _pMuxUnit;          /**< Pointer to the instance of the #MuxUnit class */

    Command*        _pCommand;          /**< Pointer to an instance of a #Command class (i.e. request) */
    Result*         _pResult;           /**< Pointer to an instance of a #Result class (i.e. reply) */
    BYTE*           _pAnalImg;          /**< Pointer to the image */

    int             _dLastCamTop;       /**< RasCam: last used TopMux */
    int             _dLastCamMas;       /**< RasCam: last used MasterMux */
    int             _dLastCamRas;       /**< RasCam: last used RasMux */
    int             _dLastLedTop;       /**< RasLed: last used TopMux */
    int             _dLastLedMas;       /**< RasLed: last used MasterMux */
    int             _dLastLedRas;       /**< RasLed: last used RasMux */

    bool            _bSameAddr;         /**< Channel request on same Mux address */
    bool            _bUseI2c;           /**< Channel request with I2c setting */

    time_t          _tClock;            /**< Time of incoming request */
    char            _cBuf[BUF_SIZE];    /**< Temporary buffer for strings */
    std::string     _sPrefix;           /**< Prefix string (name of channel) of the #_sStdout string */
    std::string     _sStdout;           /**< String used for the WindoutService of #_pServer */
    bool            _bSavedImage;       /**< True if image was saved during the request, false otherwise */
    unsigned int    _uConsGrabErr;      /**< Number of consecutive grabber errors */
    std::string     _sFirstGrabErr;     /**< First channel of consecutive grabber errors */

    bool            _bAnalServer;       /**< Run state of the concerning analysis server */
    bool            _bSoapState;        /**< Run state of the Soap-server, okay if true, unknown otherwise */
    std::string     _sSoapHost;         /**< Host on which Soap-server is running and used */
    int             _iSoapVersion;      /**< Version number of the Soap-server */
    bool            _bSpotState;        /**< Run state of the Spot-server, okay if true, unknown otherwise */
    std::string     _sSpotHost;         /**< Host on which Spot-server is running and used */
    int             _iSpotVersion;      /**< Version number of the Spot-server */
    float           _fDout[DOUT_SIZE];  /**< Array with the analysis results */
    
    void            rpcHandler();
    void            preHandleCommand();
    void            postHandleCommand();
    std::string     stringRequest();
    std::string     analType2String(const int dAnalType);
    void            dualLog(const std::string sMess);
    bool            saveImage(const std::string sImageFile);
    void            exposeImage(const std::string sChannel, const int xWidth, const int yHeight, BYTE* pImage);
    void            blackBorder();
    bool            analyseImage(BYTE* pImage);
    bool            analSoapImage(BYTE* pImage);
    bool            checkSoapServer();
    bool            startSoapServer();
    bool            probeSoapServer(const char* soapHost, const int soapPort);
    bool            analSpotImage(BYTE* pImage);
    bool            checkSpotServer();
    bool            probeSpotServer(const char* spotHost, const int spotPort);

    void            handleRequest();
    void            handleGrab();
    void            handleResetMux();
    void            handleSaveImage();

    bool            mxResetMux();
    bool            mxSwitchCam(const int topCam, const int masCam, const int rasCam);
    bool            mxSwitchLed(const int topLed, const int masLed, const int rasLed);
    bool            mxHandleI2c(const I2c& i2c);
    bool            mxExecI2c(const I2c& i2c);

    bool            isSameChannel();
    bool            isI2cChannel(const I2c& i2c);
};

#endif //RDMAIN_H
