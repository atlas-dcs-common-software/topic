/**
 *  \file   videograbber.h
 *  \brief  VideoGrabber.h: include file containing the VideoGrabber class
 *          and the IIOException class
 *  \author Robert.Hart@nikhef.nl
 *  \date   June 2016
 */

#ifndef VIDEOGRABBER_H
#define VIDEOGRABBER_H

#define IIO_DEVICE_NAME         "cf-ad9649-core-0"   /**< type of the ADC used */
#define MAX_SKIP_INIT_FRAMES    100                  /**< maximum number of frames to skip */
#define MIN_GRAB_DELAY          10                   /**< minimum grab delay: 10 msec */
#define MAX_GRAB_DELAY          5000                 /**< maximum grab delay:  5 seconds */

class Server;   // forward declaration

/**
 *  \class IIOException
 *  \brief Exception class meant for the instances of class VideoGrabber
 */
class IIOException: public std::exception
{
public:
        IIOException(const char* m) : _sMsg(m) {}    /**< IIOException constructor: #_sMsg = m */
        IIOException(const char* m1, const char* m2) : _sMsg(m1) { _sMsg += ' '; _sMsg += m2; } /**< IIOException constructor: #_sMsg = m1 + m2 */
       ~IIOException() throw() {}    /**< IIOException destructor */

        const   char*   what() const throw() { return _sMsg.c_str(); } /**< Returns the exception message */

private:
        std::string     _sMsg;       /**< The exception message */
};

/**
 *  \class VideoGrabber
 *  \brief This class deals with the ADC of the FPGA and is based on
 *         the libiio of Analog Devices. For more information look at:
 *         http://analogdevicesinc.github.io/libiio/.
 */
class VideoGrabber
{
public:
    VideoGrabber(Server* pServer, const bool bDebug, const unsigned int xWidth, const unsigned int yHeight, const unsigned int skipFrames);
   ~VideoGrabber();

    void            open(const char* host);
    void            close();
    void            grab(const unsigned long grabDelay, const bool bEnhance = false);

    bool            getStatus()             { return _bStatus;     }    /**< Returns #_bStatus */
    bool            getIsOpen()             { return _bIsOpen;     }    /**< Returns #_bIsOpen */
    unsigned int    getImgCounter()         { return _nImgCounter; }    /**< Returns #_nImgCounter */
    unsigned int    getWidth()              { return _xWidth;      }    /**< Returns #_xWidth */
    unsigned int    getHeight()             { return _yHeight;     }    /**< Returns #_yHeight */
    unsigned int    getImgSize()            { return _iImgSize;    }    /**< Returns #_iImgSize */
    unsigned int    getSkip()               { return _nSkip;       }    /**< Returns #_nSkip */
    unsigned int    getTimeout()            { return _nTimeout;    }    /**< Returns #_nTimeout */
    const unsigned  char*   getData() const { return _pData;       }    /**< Returns #_pData */

private:
    Server*                 _pServer;       /**< Pointer to the Server calling this object */
    bool                    _bStatus;       /**< True if constructor is finished */
    bool                    _bDebug;        /**< Debug flag: on if true, off otherwise */
    bool                    _bIsOpen;       /**< True if grabber is open */
    bool                    _bReOpen;       /**< True if grabber needs to be reopened */
    bool                    _bUseHost;      /**< True if remote host is used */
    std::string             _sHost;         /**< Name of host if _bUseHost is true */
    unsigned int            _xWidth;        /**< Pixel size of image horizontally */
    unsigned int            _yHeight;       /**< Pixel size vertically */
    unsigned int            _iImgSize;      /**< Pixel size of image (#_xWidth * #_yHeight) */
    unsigned char*          _pData;         /**< Buffer containing the image */
    struct   iio_context*   _pIio_ctx;      /**< Pointer to an iio_context */
    struct   iio_device*    _pIio_dev;      /**< Pointer to an iio_device */
    struct   iio_buffer*    _pIio_buf;      /**< Pointer to an iio_buffer */
    unsigned int            _nSkip;         /**< Number of frames to be skipped before grabbing */
    unsigned int            _nImgCounter;   /**< Number of successful images taken */
    unsigned int            _nTimeout;      /**< Number of failed grabs due to timeout */
};

#endif // VIDEOGRABBER_H
