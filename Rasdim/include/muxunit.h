/**
 *  \file   muxunit.h
 *  \brief  Include file containing the MuxUnit class and a lot of accompanying constants.
 *  \author Robert.Hart@nikhef.nl
 *  \date   First version 2004
 */

#define BUF_SIZE            1024        /**< Size of the temporary error and command buffer */

// fan-out of the different multiplexers
#define MAX_MMM             4           /**< mini-master-mux (predecessor of TopMux, definition not used anymore) */
#define MAX_TOPMUX          8           /**< Maximum number of TopMuxes applied at ATLAS */
#define MAX_MASTER          16          /**< Fan-out of the MasterMux */
#define MAX_RASCAM          8           /**< Fan-out of the RasMux (for cameras) */
#define MAX_RASLED          8           /**< Fan-out of the RasMux (for LEDs) */

#define CAM_OFF             MAX_RASCAM+1    /**< Value to switch off camera */
#define LED_OFF             MAX_RASLED+1    /**< Value to switch off LED */

// definitions of the I2C Setup-2 register
#define PX_DISABLE_PIXEL    0
#define PX_QUALIFY_IMAGE    1
#define PX_QUALIFY_CENTRAL  2
#define PX_PIXEL_VALID      3
#define PX_FREE_RUNNING     8

#define SH_ALL              0
#define SH_NONE             1
#define SH_SYNC_ONLY        2
#define SH_SYNC_PLUS        3
#define SH_Q1               4
#define SH_Q2               5
#define SH_Q3               6
#define SH_Q4               7

// definitions of the I2C Gain register
#define GN_G1               0
#define GN_G2               1
#define GN_G4               3
#define GN_G8               7
#define GN_G16              15

// forward declarations
class   ComPort;
class   Server;

/**
 *  \class  MuxUnit
 *  \brief  This class communicates with the firmware of the TopMux. For this purpose it
 *          uses an instance of the class #ComPort.
 *
 *  The communication with the TopMux is completely synchronous. After writing the command, the following
 *  lines are read (all lines start with a prompt #ACK):
 *  \li The echoed command.
 *  \li The result string(s) (optional, depends on the command)
 *  \li Read all lines until an empty prompt (the #ACK line).
 *
 *  On error the #_sLastError string is filled, which can be obtained by the #getLastError method.
 *
 *
 *  \warning    On the Topic Embedded Frame-grabber boards the following bugs/issues/features
 *              (concerning the communication with the TopMux) are known:
 *              \li Somehow the commands are not echoed properly.
 *              \li The firmware of the TopMux generates (not always) empty lines in between.
 *                  They are ignored in the #receiveLine method.
 *
 *  \author Robert.Hart@nikhef.nl
 *  \date   Feb 2016, implemented on TOPIC
 */
class MuxUnit
{
public:
                MuxUnit(const std::string& comPort, const long baudRate, const bool debug, Server* pServer);
               ~MuxUnit();

    bool        resetMux();
    bool        selectCam(const int topCam, const int masCam, const int rasCam);
    bool        selectLed(const int topLed, const int masLed, const int rasLed);
    bool        getMuxVersion(std::string& sVersion);
    bool        getMuxCurrent(std::string& sCurrent);
    bool        getI2cSetting(std::string& i2cset);

    bool        wrI2cSetup1(const bool normalbacklit,  const bool linearmode,
                            const bool autogain,       const bool inhibitblack,
                            const bool enableauto,     const bool horshuffle,
                            const bool vershuffle,     const bool forceblack,
                            const bool div0,           const bool div1);
    bool        wrI2cSetup2(const bool readmode,       const int  pixelmode,
                            const bool pixthreshold,   const int  shufflemode);
    bool        wrI2cSetup3(const int  pixeloffset,    const bool enablesno);               
    bool        wrI2cAnalog(const bool antiblooming,   const bool blackreference,
                            const bool whitethreshold, const bool binairisation);
    bool        wrI2cGain  (const int  gain  );
    bool        wrI2cCoarse(const int  coarse);
    bool        wrI2cFine  (const int  fine  );
    bool        wrI2cLower (const int  lower );
    bool        wrI2cUpper (const int  upper );

    bool        getStatus()    { return _bStatus;    }  /**< Return #_bStatus */
    bool        getDebug()     { return _bDebug;     }  /**< Return #_bDebug */
    std::string getLastError() { return _sLastError; }  /**< Return #_sLastError */
    std::string getComPort()   { return _sComPort;   }  /**< Return #_sComPort */
    
private:
    Server*     _pServer;           /**< Pointer to the Server instance */
    ComPort*    _pComPort;          /**< Pointer to an instance of the ComPort class */
    std::string _sLastError;        /**< String containing the last error (if any) */
    std::string _sComPort;          /**< String containing the connected COM-port (or /dev/tty...) */
    bool        _bStatus;           /**< True if the constructor finished successfully */
    bool        _bDebug;            /**< Debug flag: on if true, off otherwise */
    char        _cBuf[BUF_SIZE];    /**< Temporary buffer for the last error */
    char        _cCmd[BUF_SIZE];    /**< Temporary buffer for the command */

    bool        issueCommand(const std::string& cmd);
    bool        receiveLine(std::string& line);
    bool        handleI2CCommand(const std::string& cmd);
    bool        waitForAck(std::string& str);
};

