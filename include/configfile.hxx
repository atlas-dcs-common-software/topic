/*
 *  configfile.h ------
 */
#ifndef CONFIGFILE_H
#define CONFIGFILE_H

#define MAX_LINE        1024

class ConfigFile
{
public:
    ConfigFile();
   ~ConfigFile();

    bool            chkIniFile  (const std::string sIniFile);
    int             getIntVal   (const std::string sIniFile, const std::string sParameter, const int         dDefault);
    float           getFloatVal (const std::string sIniFile, const std::string sParameter, const float       fDefault);
    std::string     getStringVal(const std::string sIniFile, const std::string sParameter, const std::string sDefault);
};

#endif //CONFIGFILE_H
