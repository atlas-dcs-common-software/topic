/**
 *	\file	comport.h
 *	\brief	Include file containing the ComPort class\
 *	\author	Robert.Hart@nikhef.nl
 *	\date	May 2014
 */

//
#define MAX_BYTES	1024	/**< size of temporary buffer */

class Server;   // forward declaration

class ComPort
{
public:
            ComPort(const std::string& portName, const long baudRate, const bool debug, Server* pServer);
           ~ComPort();

    bool            readComPort(std::string& str);
    bool            writeComPort(const std::string& str);

    bool            getStatus()    { return _bStatus;    };
    bool            getDebug()     { return _bDebug;     };
    std::string     getLastError() { return _sLastError; };
    std::string     getComPort()   { return _sComPort;   };
    long            getBaudRate()  { return _dBaudRate;  };

    void            dbgString(const std::string& hdr, const std::string& str);

private:
    int             _Fd;
    bool            _bStatus;
    bool            _bDebug;
    Server*         _pServer;
    std::string     _sLastError;
    std::string     _sComPort;
    long            _dBaudRate;
    char            _cBuf[MAX_BYTES];
};

