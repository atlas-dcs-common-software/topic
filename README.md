
Topic: contains a set of programs to be used on the embedded Linux PCs: epc-mdt-algn-0x (1 <= x <= 8),
located inside rack Y.28-19A1 of USA15.
The most important program is the so-called Rasdim server.

The Rasdim server takes care of handling the barrel alignment channels of ATLAS. Version 8 and higher
run on an embedded dual core ARM Cortex A9 processor with a Linux 4.6.0 kernel, which is mounted on a
board manufactured by Topic Embedded Products including a Xilinx Zynq Z7015 FPGA and standard peripherals
like GBit ethernet and USB. The board is connected by 2 cables to a so-called TopMux (a multiplexer on
top of the multiplexer hierarchy).

  1. DVI cable for the video signal, which is captured and digitized by the ADC of the Xilinx FPGA.
  2. COMport to control the TopMux, like selecting camera and light source, reset the TopMux, etc.

The introduction of the board in 2016 (aka Ethernet based frame grabber ) is part of a Nikhef project,
called: Rejuvenation of the ATLAS barrel alignment controls. Detailed information can be found on the
Nikhef project management web server: https://redmine.nikhef.nl/et/projects/atlas_maintenance_rasnik

Usage
A typical select/analyze channel request triggers the server to perform the following steps:

  1. Switch on the camera and light source of the channel (optionally any I2C setting as well)
  2. Grab the image (by means of the ADC)
  3. Analyze the image and send the results back to the client

Dependencies
The Rasdim server is dependent on the following external packages:

  1. The Rasdim server communicates with its clients by means of DIM (http://dim.web.cern.ch/dim/),
     a communication package provided by CERN.
  2. The ADC of the FPGA is controlled and monitored by the libiio package of Analog Devices
     (http://analogdevicesinc.github.io/libiio).
  3. There are 2 types of analysis (modules) supported: Soap and Spot. Each type is available as server
     and the communication between the analysis servers and Rasdim is based on the libcurl package.
  4. The libconfigfile package is used to utilize a configuration file, hence -lconfigfile.

Implementation Details
The Xilinx SDK is used to (cross) compile and link the Rasdim server and other Topic programs.
