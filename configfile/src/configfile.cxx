/*
 *  configfile.cpp ------
 *  Read parameters inside a configuration file.
 *  May 2014, Robert.Hart@nikhef.nl
 */

#define _CRT_SECURE_NO_WARNINGS 1 

#include <string>
#include <stdio.h>
#include "configfile.hxx"

ConfigFile::ConfigFile()
{
}

ConfigFile::~ConfigFile()
{
}

bool ConfigFile::chkIniFile  (const std::string sIniFile)
{
    FILE    *fp;
    fp = fopen(sIniFile.c_str(), "r");
    if (fp == NULL)
        return false;
    
    fclose(fp);
    return(true);
}

int ConfigFile::getIntVal(const std::string sIniFile, const std::string sParameter, const int dDefault)
{
    FILE    *fp;
    fp = fopen(sIniFile.c_str(), "r");
    if (fp == NULL)
        return dDefault;
    
    char    sLine[MAX_LINE];
    char    sVariable[MAX_LINE];
    int     dResult;

    while( !feof(fp) )
    {
        fgets(sLine, MAX_LINE, fp);
        int n = sscanf(sLine, "%s = %d", sVariable, &dResult);
        if (n != 2)
            continue;
        if (sParameter == sVariable)
        {
            fclose(fp);
            return dResult;
        }
    }
    fclose(fp);
    return dDefault;
}

float ConfigFile::getFloatVal(const std::string sIniFile, const std::string sParameter, const float fDefault)
{
    FILE    *fp;
    fp = fopen(sIniFile.c_str(), "r");
    if (fp == NULL)
        return fDefault;
    
    char    sLine[MAX_LINE];
    char    sVariable[MAX_LINE];
    float   fResult;

    while( !feof(fp) )
    {
        fgets(sLine, MAX_LINE, fp);
        int n = sscanf(sLine, "%s = %f", sVariable, &fResult);
        if (n != 2)
            continue;
        if (sParameter == sVariable)
        {
            fclose(fp);
            return fResult;
        }
    }
    fclose(fp);
    return fDefault;
}

std::string ConfigFile::getStringVal(const std::string sIniFile, const std::string sParameter, const std::string sDefault)
{
    FILE    *fp;
    fp = fopen(sIniFile.c_str(), "r");
    if (fp == NULL)
        return sDefault;
    
    char    sLine[MAX_LINE];
    char    sVariable[MAX_LINE];
    char    sResult[MAX_LINE];

    while( !feof(fp) )
    {
        fgets(sLine, MAX_LINE, fp);
        int n = sscanf(sLine, "%s = %s", sVariable, sResult);
        if (n != 2)
            continue;
        if (sParameter == sVariable)
        {
            fclose(fp);
            return sResult;
        }
    }
    fclose(fp);
    return sDefault;
}
